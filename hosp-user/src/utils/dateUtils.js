import moment from "moment";
function computeTime(){
    const timeArr=["10:30","11:30","15:30","17:30"]
    let time = new Date().toLocaleTimeString()
    let workTimeArr=[]
    for(let i=0;i<timeArr.length;i++){
      if (compareTime(timeArr[i],time)){
        workTimeArr.push(i+1)
      }
    }
    if (workTimeArr.length===0){
      workTimeArr.push(5);
    }
    return workTimeArr
}

function dateFormat(data) {
  return moment(new Date(data).getTime()).format('YYYY-MM-DD')
}

function compareTime(t1,t2) {
    let date = new Date();
    let a = t1.split(":");
    let b = t2.split(":");
    return date.setHours(a[0],a[1]) > date.setHours(b[0],b[1]);
}




export {
    computeTime,
  dateFormat
}
