import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store";
import {Dialog} from "vant";

Vue.use(VueRouter)

const routes = [
  {
    path:'/login',
    name:'login',
    component: () => import('@/views/login/')
  },
  {
    path: '/',
    component: ()=> import('@/views/layout/'),
    children:[
      {
        path:'',//默认子路由
        name:'home',
        component:()=>import('@/views/home/')
      },
      {
        path:'/my/',//默认子路由
        name:'my',
        component:()=>import('@/views/my/'),
        meta: { requiresAuth: true }
      },
      {
        path: '/order/my/',
        name: 'myorder',
        component:()=>import('@/views/order/my/'),
      },
      {
        path:'/my/setting/',
        name: 'mysetting',
        component:()=>import('@/views/my/setting/')
      }
    ]
  },
  {
    path:'/order',
    name:'order',
    component:()=>import('@/views/order/')
  },
  {
    path:'/doctor',
    name:'doctor',
    component:()=>import('@/views/doctor/')
  },
  {
    path:'/doctor/comment',
    name: 'comment',
    component:()=>import('@/views/doctor/comment/')
  },
  {
    path:'/doctor/detail/',
    name:'doctorDetail',
    component:()=>import('@/views/doctor/detail/')
  },
  {
    path:'/patient/',
    name:'patient',
    component:()=>import('@/views/patient/')
  },
  {
    path:'/patient/add/',
    name:'patientAdd',
    component:()=>import('@/views/patient/add')
  },
  {
    path:'/patient/detail/',
    name:'patientDetail',
    component:()=>import('@/views/patient/detail')
  },
  {
    path: '/order/submit/',
    name:'orderSubmit',
    component:()=>import('@/views/order/submit/')
  },
  {
    path:'/order/detail/',
    name:'orderDetail',
    component:()=>import('@/views/order/detail/')
  },
  {
    path: '/prescription/my/',
    name:'myPrescription',
    component:()=>import('@/views/prescription/my')
  },
  {
    path: '/prescription/detail/',
    name:'prescriptionDetail',
    component:()=>import('@/views/prescription/detail')
  },
  {
    path: '/outpatientPayment/my/',
    name:'myOutpatientPayment',
    component:()=>import('@/views/outpatientPayment/my')
  },
  {
    path: '/outpatientPayment/detail/',
    name:'outpatientPaymentDetail',
    component:()=>import('@/views/outpatientPayment/detail')
  }
]

const router = new VueRouter({
  mode:'history',
  routes:routes
})

router.beforeEach((to, from, next) => {
  if (to.name === 'login' || !to.meta.requiresAuth) {
    return next()
  }

  if (store.state.user) {
    return next()
  }

  Dialog.confirm({
    title: '该功能需要登录，确认登录吗？'
  }).then(() => {
    // next({
    //   name: 'login',
    //   query: {
    //     redirect: from.fullPath
    //   }
    // })
    next('/login')
  }).catch(() => {
    // on cancel
  })
})

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject){
    return originalPush.call(this, location, onResolve, onReject)
  }
  return originalPush.call(this, location).catch(err => err)
}

export default router

