import request from '@/utils/request'

export default {
  //添加评论
  add(comment){
    return request({
      url:`comment/add`,
      method:'post',
      data:comment
    })
  },
  //根据医生编号查询评论
  findByDocId(docId){
    return request({
      url:`comment/findByDocId/${docId}`,
      method:'get',
    })
  },
  findPageByDocId(docId, page, limit) {
    return request({
      url:`comment/findPageByDocId/${docId}/${page}/${limit}`,
      method:'get',
    })
  },
  getIntegratedInformation(docId){
    return request({
      url:`comment/findNumberOfCommentAndIntegratedRate/${docId}`,
      method:'get',
    })
  }
}
