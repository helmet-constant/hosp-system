import request from '@/utils/request'

export default {
  findByDepcode(depcode){
    return request({
      url:`doctor/findByDepcode/${depcode}`,
      method:'get'
    })
  },
  findById(id){
    return request({
      url:`doctor/findById/${id}`,
      method:'get'
    })
  },
}
