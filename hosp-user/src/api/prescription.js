import request from '@/utils/request'

export default {
  getPastPrescriptionList(searchObj){
    return request({
      url:`prescription/find`,
      method:'post',
      data:searchObj
    })
  },
  findById(prescriptionId){
    return request({
      url:`prescription/findById/${prescriptionId}`,
      method:'get',
    })
  },
  getPrescriptionDetail(prescriptionId) {
    return request({
      url:`prescription/findDetail/${prescriptionId}`,
      method:'get',
    })
  },
  pay(prescriptionId){
    return request({
      url:`prescription/pay/${prescriptionId}`,
      method:'get',
    })
  }
}
