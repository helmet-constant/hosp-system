import request from '@/utils/request'

export default {
  //发送验证码
  sendCode(phone){
    return request({
      url:`sys/user/sendCode/${phone}`,
      method:'get'
    })
  },
  //注册
  register(user,code){
    return request({
      url:`sys/user/register/${code}`,
      method:'post',
      data:user
    })
  },
  //登录
  login(user){
    return request({
      url:`sys/user/login`,
      method:'post',
      data:user
    })
  },
  //注销
  logout(){
    return request({
      url:`sys/user/logout`,
      method:'get'
    })
  },
  phoneLogin(user) {
    return request({
      url:`sys/user/phoneLogin`,
      method:'post',
      data:user
    })
  },
  updatePass(passwordDto){
    return request({
      url:`sys/user/updatePass`,
      method:'post',
      data:passwordDto
    })
  }
}
