import request from '@/utils/request'

export default {
  add(orderInfo){
    return request({
      url:`order/add`,
      method:'post',
      data:orderInfo
    })
  },
  findById(id){
    return request({
      url:`order/findById/${id}`,
      method:'get'
    })
  },
  findByObj(searchObj){
    return request({
      url:`order/findByObj`,
      method:'post',
      data:searchObj
    })
  },
  cancel(orderId,reason){
    return request({
      url:`order/cancel/${orderId}/${reason}`,
      method:'get',
    })
  },
  pay(orderId){
    return request({
      url:`order/pay/${orderId}`,
      method:'get',
    })
  }
}
