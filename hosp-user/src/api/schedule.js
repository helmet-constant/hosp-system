import request from '@/utils/request'

export default {
  getRecently(depcode){
    return request({
      url:`sche/findRecently/${depcode}`,
      method:'get'
    })
  },
  get14Day(){
    return request({
      url:`api/sche/find14Day`,
      method:'get'
    })
  },
  find(searchObj){
    return request({
      url:`sche/findByDepcodeAndWorkDate`,
      method:'get',
      params:searchObj
    })
  },
  findByDocId(id){
    return request({
      url:`api/sche/findByDocId/${id}`,
      method:'get'
    })
  },
  getRecentDayByDocId(docId) {
    return request({
      url:`/sche/findRecentDayByDocId/${docId}`,
      method:'get'
    })
  },
  findByCond(searchObj){
    return request({
      url:`sche/find`,
      method:'post',
      data:searchObj
    })
  }
}
