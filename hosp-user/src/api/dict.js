import request from '@/utils/request'

export default {
  //数据字典列表
  dictList(id){
    return request({
      url:`dict/findChildren/${id}`,
      method:'get'
    })
  },
  getDictName(value){
    return request({
      url:`dict/getDictName/${value}`,
      method:'get'
    })
  }
}
