import request from '@/utils/request'

export default {
  add(patient){
    return request({
      url:`patient/add`,
      method:'post',
      data:patient
    })
  },
  findByUserId(userId){
    return request({
      url:`patient/findByUserById/${userId}`,
      method:'get'
    })
  },
  findById(id){
    return request({
      url:`patient/findById/${id}`,
      method:'get'
    })
  },
  update(patient){
    return request({
      url:`patient/update`,
      method:'post',
      data:patient
    })
  },
  findFirst(userId){
    return request({
      url:`patient/findFirstByUserId/${userId}`,
      method:'get'
    })
  },
  findByCardNo(cardNo){
    return request({
      url:`patient/findByCardNo/${cardNo}`,
      method:'get'
    })
  }
}
