import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
//导入字体图标
import './assets/fonts/iconfont.css'
import store from './store'
//导入全局样式表
import './assets/css/global.css'
import global from './globalFun'



Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
