import Vue from 'vue'
import Vuex from 'vuex'
import {getItem,setItem} from "@/utils/storage";

Vue.use(Vuex)
const USER_KEY = 'user'
export default new Vuex.Store({
  state: {
    user: getItem(USER_KEY),//当前登录用户的登陆状态（token等数据）
    // 菜单栏数据
    menuList:  [],
    // 权限数据
    permList:  [],
    hasRoute: false,
    isAuth:false
  },
  getters: {

  },
  mutations: {
    setUser (state, data) {
      state.user = data
      // 为了防止页面刷新数据丢失，我们还需要把数据放到本地存储中，这里仅仅是为了持久化数据
      setItem(USER_KEY, state.user)
    },
    changeAuthStatus(state,status){
      state.isAuth = status
    },
    removeUser(state,data){
      state.user = null
      state.menuList=[]
      state.permList=[]
      state.hasRoute=false
      state.isAuth=false
      window.localStorage.clear()
    },
    changeRouteStatus(state, hasRoute) {
      state.hasRoute = hasRoute
      sessionStorage.setItem("hasRoute", hasRoute)
    },
    setMenuList(state, menus) {
      state.menuList = menus
    },
    setPermList(state, authoritys) {
      state.permList = authoritys
    },

  },
  actions: {
  },
  modules: {
  }
})