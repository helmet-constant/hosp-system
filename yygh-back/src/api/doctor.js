import request from '@/utils/request'

export default {
    //保存医生信息
    save(doctor){
        return request({
            url:`doctor/save`,
            method:'post',
            data:doctor
        })
    },
    //修改医生信息
    update(doctor){
        return request({
            url:`doctor/update`,
            method:'post',
            data:doctor
        })
    },
    //根据用户id获取医生信息 原本getDocInfo
    findByUserId(id){
        return request({
            url:`doctor/findByUserId/${id}`,
            method:'get',
        })
    },
    findByDepcode(id){
        return request({
            url:`doctor/findByDepcode/${id}`,
            method:'get',
        })
    },
    list(searchForm){
        return request({
            url:`doctor/list`,
            method:'get',
            params:searchForm
        })
    },
    //更新审核状态
    updateAuth(authInfo){
        return request({
            url:`doctor/updateAuthStatus`,
            method:'post',
            data:authInfo
        })
    },
    getAdvice(id){
        return request({
            url:`doctor/getAdvice/${id}`,
            method:'get'
        })
    },
    isAuth(){
        return request({
            url:`doctor/isAuth`,
            method:'get'
        })
    }

}
