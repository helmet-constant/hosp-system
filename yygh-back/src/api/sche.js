import request from '@/utils/request'

export default {
    add(sche){
        return request({
            url:`sche/add`,
            method:'post',
            data:sche
        })
    },
    update(sche){
        return request({
            url:`sche/update`,
            method:'post',
            data:sche
        })
    },
    delete(id){
        return request({
            url:`sche/delete/${id}`,
            method:'delete'
        })
    },
    findSevenDay(){
        return request({
            url:`sche/find14Day`,
            method:'get'
        })
    },
    find(searchObj){
        return request({
            url:`sche/find`,
            method:'post',
            data:searchObj
        })
    },
    findByDepcodeAndWorkDate(searchObj){
        return request({
            url:`sche/findByDepcodeAndWorkDate`,
            method:'get',
            params:searchObj
        })
    },
    info(id){
        return request({
            url:`sche/info/${id}`,
            method:'get',
        })
    }
}
