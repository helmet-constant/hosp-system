import request from '@/utils/request'

export default {
    generate(prescription,prescriptionDetailList){
        return request({
            url:`prescription/generate`,
            method:'post',
            data:{prescription:prescription,prescriptionDetailList:prescriptionDetailList}
        })
    },
    getPastPrescriptionList(searchObj){
        return request({
            url:`prescription/find`,
            method:'post',
            data:searchObj
        })
    },
    getPastPrescriptionDetail(prescriptionId) {
        return request({
            url:`prescription/findDetail/${prescriptionId}`,
            method:'get',
        })
    },
    getById(prescriptionId){
        return request({
            url:`prescription/findById/${prescriptionId}`,
            method:'get',
        })
    },
    notifyMedicationCollection(prescriptionId,userId){
        return request({
            url:`prescription/notifyMedicationCollection/${prescriptionId}/${userId}`,
            method:'get',
        })
    },
    finishTakeMedicine(prescriptionId, userId) {
        return request({
            url:`prescription/finishTakeMedicine/${prescriptionId}/${userId}`,
            method:'get',
        })
    }
}

