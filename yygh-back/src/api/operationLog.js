import request from '@/utils/request'

export default {
    //获取日志列表
    getOperationLog(searchObj){
        return request({
            url:`operateLog/page`,
            params:searchObj,
            method:'get'
        })
    },
    info(id){
        return request({
            url:`operateLog/info/${id}`,
            method:'get'
        })
    },
    export(searchObj){
        return request({
            url:`operateLog/export`,
            params:searchObj,
            method:'get',
            responseType:"blob"
        })
    }
}
