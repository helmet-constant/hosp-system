import request from '@/utils/request'

export default {
    //获取菜单列表
    findChildData(id){
        return request({
            url:`dict/findChildren/${id}`,
            method:'get',
        })
    },
}
