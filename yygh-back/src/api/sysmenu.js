import request from '@/utils/request'

export default {
    //查询所有权限
    find(){
        return request({
            url:`sys/menu/list`,
            method:'get',
        })
    },
    save(editForm){
        return request({
            url:`sys/menu/save`,
            method:'post',
            data:editForm
        })
    },
    update(editForm){
        return request({
            url:`sys/menu/update`,
            method:'post',
            data:editForm
        })
    },
    info(id){
        return request({
            url:`sys/menu/info/${id}`,
            method:'get',
        })
    },
    delete(id){
        return request({
            url:`sys/menu/delete/${id}`,
            method:'post',
        })
    },
    nav(){
        return request({
            url:`sys/menu/nav`,
            method:'get',
        })
    }
}