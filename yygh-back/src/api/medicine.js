import request from '@/utils/request'

export default {
    find(page,limit,queryInfo){
        return request({
            url:`med/find/${page}/${limit}`,
            method:'get',
            params:queryInfo
        })
    },
    add(medicine){
        return request({
            url:`med/add`,
            method:'post',
            data:medicine
        })
    },
    update(medicine){
        return request({
            url:`med/update`,
            method:'post',
            data:medicine
        })
    },
    delete(){

    },
    findWithNoPage(searchObj){
        return request({
            url:`med/find`,
            method:'get',
            params:searchObj
        })
    },
    info(id){
        return request({
            url:`med/info/${id}`,
            method:'get',
        })
    }
}
