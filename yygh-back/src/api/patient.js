import request from '@/utils/request'

export default {
    findByDocIdAndWorkDate(docId){
        return request({
            url:`patient/findByDocIdAndWorkDate/${docId}`,
            method:'get'
        })
    },
    getPatientList(docId){
        return request({
            url:`patient/findFromRedis/${docId}`,
            method:'get'
        })
    },
    patientOvertime(patientId,docId) {
        return request({
            url:`patient/patientovertime/${patientId}/${docId}`,
            method:'get'
        })
    },
    getOvertimePatient(docId){
        return request({
            url:`patient/findOvertimePatient/${docId}`,
            method:'get'
        })
    },
    patientVisit(type,patientId,docId){
        return request({
            url:`patient/patientVisit/${type}/${patientId}/${docId}`,
            method:'get'
        })
    },
    getVisitPatient(docId){
        return request({
            url:`patient/findVisitPatient/${docId}`,
            method:'get'
        })
    },
    //根据药库端的id获取对应的抓药列表
    getMedicatedPatientList(userId){
        return request({
            url:`patient/findMedicatedPatientList/${userId}`,
            method:'get',
        })
    },
    //根据药库段id获取对应的待取药患者
    getWaitPatientList(userId) {
        return request({
            url:`patient/findWaitPatientList/${userId}`,
            method:'get',
        })
    }
}

