import request from '@/utils/request'

export default {
    patientRegistration(cardNo){
        return request({
            url:`order/patientRegistration/${cardNo}`,
            method:'get'
        })
    },
    registerOnSite(orderInfo){
        return request({
            url:`order/registerOnSite`,
            method:'post',
            data:orderInfo
        })
    }
}
