import request from '@/utils/request'

export default {
    find(page,limit,queryInfo){
        return request({
            url:`notice/find/${page}/${limit}`,
            method:'get',
            params:{queryInfo}
        })
    },
    delete(id){
        return request({
            url:`notice/delete/${id}`,
            method:'delete'
        })
    },
    add(notice){
        return request({
            url:`notice/add`,
            method:'post',
            data:notice
        })
    },
}
