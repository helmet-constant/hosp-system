import request from '@/utils/request'

export default {
    //查询所有权限
    find(params){
        return request({
            url:`sys/role/list`,
            method:'get',
            params:params
        })
    },
    info(id){
        return request({
            url:`sys/role/info/${id}`,
            method:'get',
        })
    },
    save(editForm){
        return request({
            url:`sys/role/save`,
            method:'post',
            data:editForm
        })
    },
    update(editForm){
        return request({
            url:`sys/role/update`,
            method:'post',
            data:editForm
        })
    },
    delete(ids){
        return request({
            url:`sys/role/delete`,
            method:'post',
            data:ids
        })
    },
    perm(id,menuIds){
        return request({
            url:`sys/role/perm/${id}`,
            method:'post',
            data:menuIds
        })
    }
}