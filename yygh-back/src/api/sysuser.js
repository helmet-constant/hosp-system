import request from '@/utils/request'

export default {
    //登录
    login(user){
        return request({
            url:`sys/user/login`,
            method:'post',
            data:user
        })
    },
    //查询所有权限
    find(params){
        return request({
            url:`sys/user/list`,
            method:'get',
            params:params
        })
    },
    info(id){
        return request({
            url:`sys/user/info/${id}`,
            method:'get',
        })
    },
    delete(ids){
        return request({
            url:`sys/user/delete`,
            method:'post',
            data:ids
        })
    },
    save(editForm){
        return request({
            url:`sys/user/save`,
            method:'post',
            data:editForm
        })
    },
    update(editForm){
        return request({
            url:`sys/user/update`,
            method:'post',
            data:editForm
        })
    },
    repass(userId){
        return request({
            url:`sys/user/repass/${userId}`,
            method:'post'
            }
        )
    },
    role(userId,roleIds){
        return request({
            url:`sys/user/role/${userId}`,
            method:'post',
            data:roleIds
        })
    },
    logout(){
        return request({
            url:`sys/user/logout`,
            method:'get',
        })
    },
    updatePass(passwordDto){
        return request({
            url:`sys/user/updatePass`,
            method:'post',
            data:passwordDto
        })
    }
}