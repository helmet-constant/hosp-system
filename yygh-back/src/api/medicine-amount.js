import request from "@/utils/request";

export default {
    add(stock){
        return request({
            url:`med/amount/add`,
            method:'post',
            data:stock
        })
    },
    update(stock){
        return request({
            url:`med/amount/update`,
            method:'post',
            data:stock
        })
    },
    find(medId){
        return request({
            url:`med/amount/find/${medId}`,
            method:'get'
        })
    },
    delete(id){
        return request({
            url:`med/amount/delete/${id}`,
            method:'delete'
        })
    },
    info(id){
        return request({
            url:`med/amount/info/${id}`,
            method:'get',
        })
    }
}