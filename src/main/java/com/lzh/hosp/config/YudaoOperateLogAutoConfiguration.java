//package com.lzh.hosp.config;
//
//import com.lzh.hosp.aop.OperateLogAspect;
//import com.lzh.hosp.service.OperateLogApi;
//import com.lzh.hosp.service.OperateLogFrameworkService;
//import com.lzh.hosp.service.impl.OperateLogFrameworkServiceImpl;
//import org.springframework.context.annotation.Bean;
////import org.springframework.boot.autoconfigure.AutoConfiguration;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class YudaoOperateLogAutoConfiguration {
//
//    @Bean
//    public OperateLogAspect operateLogAspect() {
//        return new OperateLogAspect();
//    }
//
//    @Bean
//    public OperateLogFrameworkService operateLogFrameworkService(OperateLogApi operateLogApi) {
//        return new OperateLogFrameworkServiceImpl(operateLogApi);
//    }
//
//}
