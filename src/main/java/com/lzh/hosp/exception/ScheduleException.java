package com.lzh.hosp.exception;

import com.lzh.hosp.model.ResultCodeEnum;
import lombok.Data;

/**
 * 自定义全局异常类
 *
 * @author lzh
 */
@Data
public class ScheduleException extends RuntimeException {

    private Integer code;

    /**
     * 通过状态码和错误消息创建异常对象
     * @param message
     * @param code
     */
    public ScheduleException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    /**
     * 接收枚举类型对象
     * @param resultCodeEnum
     */
    public ScheduleException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    @Override
    public String toString() {
        return "ScheduleException{" +
                "code=" + code +
                ", message=" + this.getMessage() +
                '}';
    }
}
