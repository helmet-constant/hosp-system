package com.lzh.hosp.mapper;

import com.lzh.hosp.model.SystemOperateLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【system_operate_log(操作日志记录)】的数据库操作Mapper
* @createDate 2023-06-10 18:41:36
* @Entity com.lzh.hosp.model.SystemOperateLog
*/
@Mapper
public interface SystemOperateLogMapper extends BaseMapper<SystemOperateLog> {

}




