package com.lzh.hosp.mapper;

import com.lzh.hosp.model.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【sys_user_role】的数据库操作Mapper
* @createDate 2023-05-04 22:18:46
* @Entity com.lzh.hosp.model.SysUserRole
*/
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}




