package com.lzh.hosp.mapper;

import com.lzh.hosp.model.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【sys_role】的数据库操作Mapper
* @createDate 2023-05-04 22:18:20
* @Entity com.lzh.hosp.model.SysRole
*/
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}




