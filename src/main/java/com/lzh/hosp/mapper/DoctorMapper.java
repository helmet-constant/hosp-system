package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Doctor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【doctor】的数据库操作Mapper
* @createDate 2023-03-16 09:33:52
* @Entity com.lzh.hosp.model.Doctor
*/
@Mapper
public interface DoctorMapper extends BaseMapper<Doctor> {

}




