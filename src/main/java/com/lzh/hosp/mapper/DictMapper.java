package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【dict(组织架构表)】的数据库操作Mapper
* @createDate 2023-03-15 12:14:44
* @Entity com.lzh.hosp.model.Dict
*/
@Mapper
public interface DictMapper extends BaseMapper<Dict> {

}




