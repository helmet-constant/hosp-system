package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【comment】的数据库操作Mapper
* @createDate 2023-04-14 19:58:22
* @Entity com.lzh.hosp.model.Comment
*/
@Mapper
public interface CommentMapper extends BaseMapper<Comment> {

    //根据医生id查询好评数量
    Integer selectNumberOfPositiveComment(Long docId);

}




