package com.lzh.hosp.mapper;

import com.lzh.hosp.model.OrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【order_info(订单表)】的数据库操作Mapper
* @createDate 2023-03-26 11:15:23
* @Entity com.lzh.hosp.model.OrderInfo
*/
@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}




