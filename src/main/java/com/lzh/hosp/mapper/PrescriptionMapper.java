package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Prescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【prescription】的数据库操作Mapper
* @createDate 2023-04-08 11:02:01
* @Entity com.lzh.hosp.model.Prescription
*/
@Mapper
public interface PrescriptionMapper extends BaseMapper<Prescription> {

    //根据医生id查询接诊数量
    Integer selectNumberOfVisitsReceived(Long docId);

}




