package com.lzh.hosp.mapper;

import com.lzh.hosp.model.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【sys_role_menu】的数据库操作Mapper
* @createDate 2023-05-04 22:18:31
* @Entity com.lzh.hosp.model.SysRoleMenu
*/
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}




