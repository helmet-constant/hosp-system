package com.lzh.hosp.mapper;

import com.lzh.hosp.model.MedicineWarehouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【medicine_warehouse】的数据库操作Mapper
* @createDate 2023-03-15 12:16:20
* @Entity com.lzh.hosp.model.MedicineWarehouse
*/
@Mapper
public interface MedicineWarehouseMapper extends BaseMapper<MedicineWarehouse> {

    //根据药品id查询出所有的库存
    Integer getAllAmountByMedId(Long medId);

}




