package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【notice】的数据库操作Mapper
* @createDate 2023-03-14 21:34:11
* @Entity com.lzh.hosp.model.Notice
*/
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {

}




