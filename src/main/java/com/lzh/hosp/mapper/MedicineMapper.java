package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Medicine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【medicine】的数据库操作Mapper
* @createDate 2023-03-15 12:13:46
* @Entity com.lzh.hosp.model.Medicine
*/
@Mapper
public interface MedicineMapper extends BaseMapper<Medicine> {

}




