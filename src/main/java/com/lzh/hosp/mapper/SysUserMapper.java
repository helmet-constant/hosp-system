package com.lzh.hosp.mapper;

import com.lzh.hosp.model.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* @author 59399
* @description 针对表【sys_user】的数据库操作Mapper
* @createDate 2023-05-04 22:17:20
* @Entity com.lzh.hosp.model.SysUser
*/
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    List<Long> getNavMenuIds(Long userId);

    List<SysUser> listByMenuId(Long menuId);
}




