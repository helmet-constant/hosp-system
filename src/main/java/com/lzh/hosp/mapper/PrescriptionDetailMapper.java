package com.lzh.hosp.mapper;

import com.lzh.hosp.model.PrescriptionDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【prescription_detail】的数据库操作Mapper
* @createDate 2023-04-08 11:02:10
* @Entity com.lzh.hosp.model.PrescriptionDetail
*/
@Mapper
public interface PrescriptionDetailMapper extends BaseMapper<PrescriptionDetail> {

}




