package com.lzh.hosp.mapper;

import com.lzh.hosp.model.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 59399
* @description 针对表【sys_menu】的数据库操作Mapper
* @createDate 2023-05-04 22:18:24
* @Entity com.lzh.hosp.model.SysMenu
*/
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}




