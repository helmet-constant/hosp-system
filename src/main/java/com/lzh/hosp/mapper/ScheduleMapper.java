package com.lzh.hosp.mapper;

import com.lzh.hosp.model.Schedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author 59399
* @description 针对表【schedule(医生日程安排表)】的数据库操作Mapper
* @createDate 2023-03-16 20:59:23
* @Entity com.lzh.hosp.model.Schedule
*/@Mapper
public interface ScheduleMapper extends BaseMapper<Schedule> {
    List<Long> selectDocIdByDepCodeAndDate(String date,Long docId);

    List<Long> selectIllegal(String date,@Param("workTimeList") List<Integer> workTimeList);
}




