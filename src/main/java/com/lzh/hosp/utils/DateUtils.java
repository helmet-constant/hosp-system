package com.lzh.hosp.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateUtils {
    public static int[] workTimeArr = {1030,1130,1530,1730};
    public static boolean isEffectiveDay(Date date){
        DateTime scheDate = new DateTime(date);
        DateTime today = new DateTime();
        DateTime dateTime = today.plusDays(14);
        if (scheDate.isBefore(today)||scheDate.isAfter(dateTime)){
            return false;
        }
        return true;
    }

    public static List<Integer> computeTime(){
        Date d1 = new Date();
        SimpleDateFormat f = new SimpleDateFormat("HHmm"); //格式化为 hhmmss
        int d1Number = Integer.parseInt(f.format(d1).toString()); //将第一个时间格式化后转为int
        List<Integer> workTimeList = new ArrayList<>();
        for (int i=0;i<workTimeArr.length;i++){
            if (d1Number>workTimeArr[i]){
                workTimeList.add(i+1);
            }
        }
        return workTimeList;
    }



    public static void main(String[] args) {
        List<Integer> integers = DateUtils.computeTime();
        System.out.println(integers);
    }
}
