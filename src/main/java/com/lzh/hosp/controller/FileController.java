package com.lzh.hosp.controller;


import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.service.FileService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
@Tag(name="文件管理")
public class FileController {

    @Autowired
    private FileService fileService;

    //上传文件到阿里云
    @PostMapping("fileUpload")
    public ResponseResult fileUpload(@RequestParam MultipartFile file){
        //获取上传文件
        String url = fileService.upload(file);
        return ResponseResult.ok(url);
    }

}
