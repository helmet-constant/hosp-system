package com.lzh.hosp.controller;

import com.alibaba.fastjson.JSON;
import com.lzh.hosp.model.*;
import com.lzh.hosp.model.dto.PrescriptionQueryDto;
import com.lzh.hosp.service.DictService;
import com.lzh.hosp.service.DoctorService;
import com.lzh.hosp.service.PrescriptionDetailService;
import com.lzh.hosp.service.PrescriptionService;
import com.lzh.hosp.websocket.WebSocketServer;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/prescription")
@Tag(name="处方管理")
public class PrescriptionController {

    @Autowired
    private PrescriptionService prescriptionService;

    @Autowired
    private PrescriptionDetailService prescriptionDetailService;

    @Autowired
    private WebSocketServer webSocketServer;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private DictService dictService;

    @Autowired
    private HttpServletRequest request;

    //生成处方
    @PostMapping("/generate")
    @PreAuthorize("hasAuthority('sys:prescription:add')")
    public ResponseResult generatePrescription(@RequestBody Map<String,Object> models){
        String prescriptionJson = JSON.toJSONString(models.get("prescription"));
        String prescriptionDetailJson = JSON.toJSONString(models.get("prescriptionDetailList"));

        //将json转换为对象
        Prescription prescription = JSON.parseObject(prescriptionJson,Prescription.class);
        List<PrescriptionDetail> prescriptionDetailList = JSON.parseArray(prescriptionDetailJson,PrescriptionDetail.class);

        boolean flag = prescriptionService.savePrescription(prescription,prescriptionDetailList);
        return ResponseResult.ok(flag);
    }

    //多条件查询查询处方
    @PostMapping("/find")
    public ResponseResult find(@RequestBody PrescriptionQueryDto prescriptionQueryDto){
        List<Prescription> list = prescriptionService.getPrescription(prescriptionQueryDto);
        return ResponseResult.ok(list);
    }

    //根据处方编号查询处方
    @GetMapping("/findById/{prescriptionId}")
    public ResponseResult findById(@PathVariable String prescriptionId){
        Prescription prescription = prescriptionService.getById(prescriptionId);
        //设置其他值
        //根据docId查出医生名和科室编号
        Integer docId = prescription.getDocId();
        Doctor doctor = doctorService.getById(docId);
        //根据科室编号查询科室名
        String depname = dictService.getDictName(doctor.getDepcode());
        String docname = doctor.getUsername();

        Map<String,Object> params = new HashMap<>();
        params.put("depname",depname);
        params.put("docname",docname);
        prescription.setParams(params);

        return ResponseResult.ok(prescription);
    }

    //根据处方id查询处方明细
    @GetMapping("/findDetail/{prescriptionId}")
    public ResponseResult findDetail(@PathVariable String prescriptionId){
        List<PrescriptionDetail> list = prescriptionDetailService.getByPrescriptionId(prescriptionId);
        return ResponseResult.ok(list);
    }

    //支付处方金额
    @GetMapping("/pay/{prescriptionId}")
    public ResponseResult pay(@PathVariable String prescriptionId){
        boolean flag = prescriptionService.pay(prescriptionId);
        return ResponseResult.ok(flag);
    }

    //完成取药，并通知电子显示大屏显示取药病人
    @GetMapping("/notifyMedicationCollection/{prescriptionId}/{userId}")
    @PreAuthorize("hasAuthority('sys:drugstore:notify')")
    public ResponseResult notifyMedicationCollection(@PathVariable String prescriptionId,@PathVariable Long userId){
        boolean flag = prescriptionService.finishMedicationCollection(prescriptionId,userId);
        return ResponseResult.ok(flag);
    }

    //病人取药完成
    @GetMapping("finishTakeMedicine/{prescriptionId}/{userId}")
    @PreAuthorize("hasAuthority('sys:drugstore:finish')")
    public ResponseResult finishTakeMedicine(@PathVariable String prescriptionId,@PathVariable Long userId){
        boolean flag = prescriptionService.finishTakeMedicine(prescriptionId,userId);
        return ResponseResult.ok(flag);
    }
}
