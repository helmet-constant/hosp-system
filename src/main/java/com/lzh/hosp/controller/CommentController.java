package com.lzh.hosp.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.model.Comment;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.service.CommentService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comment")
@Tag(name="评论管理")
public class CommentController {

    @Autowired
    private CommentService commentService;

    //添加评论
    @PostMapping("/add")
    public ResponseResult add(@RequestBody Comment comment){
        boolean flag = commentService.saveComment(comment);
        return ResponseResult.ok(flag);
    }

    //根据医生编号查询所有评论
    @GetMapping("findByDocId/{docId}")
    public ResponseResult findByDocId(@PathVariable Long docId){
        List<Comment> list = commentService.getByDocId(docId);
        return ResponseResult.ok(list);
    }

    //根据医生编号分页查询评论
    @GetMapping("findPageByDocId/{docId}/{page}/{limit}")
    public ResponseResult findPageByDocId(@PathVariable Long docId,@PathVariable Long page,@PathVariable Long limit){
        IPage<Comment> iPage = commentService.getPageByDocId(docId,page,limit);
        return ResponseResult.ok(iPage);
    }

    //根据医生编号查询评价数量和综合评分
    @GetMapping("findNumberOfCommentAndIntegratedRate/{docId}")
    public ResponseResult findNumberOfCommentAndIntegratedRate(@PathVariable Integer docId){
        Map<String,Object> params = commentService.getNumberOfCommentAndIntegratedRate(docId);
       return ResponseResult.ok(params);
    }
}
