package com.lzh.hosp.controller;

import com.lzh.hosp.model.Dict;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.service.DictService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dict")
@Tag(name="字典管理")
public class DictController {

    @Autowired
    private DictService dictService;

    //根据数据id查询子数据列表
    @GetMapping("findChildren/{id}")
    public ResponseResult findChildren(@PathVariable Long id) {
        List<Dict> list = dictService.listChildren(id);
        return ResponseResult.ok(list);
    }

    //根据value查询
    @GetMapping("getDictName/{value}")
    public String getName(@PathVariable Long value) {
        return dictService.getDictName(value);
    }

    //根据value查询出父节点的名字
    @GetMapping("getParentName/{value}")
    public String getParentName(@PathVariable Long value){
        return dictService.getParentName(value);
    }

}
