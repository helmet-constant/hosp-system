package com.lzh.hosp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.model.Medicine;
import com.lzh.hosp.model.MedicineWarehouse;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.model.dto.MedicineQueryDto;
import com.lzh.hosp.service.MedicineService;
import com.lzh.hosp.service.MedicineWarehouseService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/med")
@Tag(name="药品管理")
public class MedicineController {

    @Autowired
    private MedicineService medicineService;

    @Autowired
    private MedicineWarehouseService medicineWarehouseService;

    //根据id查询药品
    @GetMapping("/info/{id}")
    public ResponseResult find(@PathVariable Long id){
        Medicine medicine = medicineService.getById(id);
        return ResponseResult.ok(medicine);
    }

    //更新药品
    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:medicine:update')")
    public ResponseResult update(@RequestBody @Validated Medicine medicine){
        medicine.setUpdateTime(new Date());
        boolean flag = medicineService.updateById(medicine);
        return flag?ResponseResult.ok():ResponseResult.fail("更新药品失败");
    }

    //条件查询药品，带分页
    @GetMapping("/find/{page}/{limit}")
    public ResponseResult find(@PathVariable Long page,
                               @PathVariable Long limit,
                               MedicineQueryDto medicineQueryDto){
        IPage<MedicineQueryDto> iPage =  medicineService.list(medicineQueryDto,page,limit);
        return ResponseResult.ok(iPage);
    }

    //条件查询余量充足的药品，无分页
    @GetMapping("/find")
    public ResponseResult find(MedicineQueryDto medicineQueryVo){
        List<Medicine> list = medicineService.list(medicineQueryVo);
        return ResponseResult.ok(list);
    }

    //添加药品
    @PostMapping("/add")
    @PreAuthorize("hasAuthority('sys:medicine:add')")
    public ResponseResult add(@RequestBody @Validated Medicine medicine){
        medicine.setCreatedTime(new Date());
        medicine.setUpdateTime(new Date());
        medicineService.save(medicine);
        return ResponseResult.ok();
    }

    //根据库存id查询库存
    @GetMapping("/amount/info/{id}")
    public ResponseResult amountInfo(@PathVariable Long id){
        MedicineWarehouse medicineWarehouse = medicineWarehouseService.getById(id);
        //设置其他值
        Map<String,Object> params = new HashMap<>();
        params.put("medName",medicineService.getById(medicineWarehouse.getMedId()).getMedName());
        medicineWarehouse.setParams(params);
        return ResponseResult.ok(medicineWarehouse);
    }

    //根据药品id查询库存
    @GetMapping("/amount/find/{medId}")
    public ResponseResult findAmount(@PathVariable Long medId){
        QueryWrapper<MedicineWarehouse> wrapper = new QueryWrapper<>();
        wrapper.eq("med_id",medId);
        List<MedicineWarehouse> list = medicineWarehouseService.list(wrapper);
        //设置其他值
        for (MedicineWarehouse medicineWarehouse : list) {
            Map<String,Object> params = new HashMap<>();
            params.put("medName",medicineService.getById(medicineWarehouse.getMedId()).getMedName());
            medicineWarehouse.setParams(params);
        }
        return ResponseResult.ok(list);
    }

    //添加库存
    @PostMapping("/amount/add")
    @PreAuthorize("hasAuthority('sys:medicinewarehouse:add')")
    public ResponseResult addAmount(@RequestBody @Validated MedicineWarehouse medicineWarehouse){
        medicineWarehouse.setCreatedTime(new Date());
        medicineWarehouseService.save(medicineWarehouse);
        return ResponseResult.ok();
    }

    //修改库存
    @PostMapping("/amount/update")
    @PreAuthorize("hasAuthority('sys:medicinewarehouse:update')")
    public ResponseResult updateAmount(@RequestBody @Validated MedicineWarehouse medicineWarehouse){
        boolean update = medicineWarehouseService.updateById(medicineWarehouse);
        return update?ResponseResult.ok():ResponseResult.fail("更新库存失败");
    }

    //删除库存
    @DeleteMapping("/amount/delete/{id}")
    public ResponseResult deleteWareHouse(@PathVariable Integer id){
        boolean b = medicineWarehouseService.removeById(id);
        return b?ResponseResult.ok():ResponseResult.fail("删除失败");
    }

}
