package com.lzh.hosp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzh.hosp.model.Patient;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.model.Schedule;
import com.lzh.hosp.service.PatientService;
import com.lzh.hosp.service.ScheduleService;
import com.lzh.hosp.utils.RedisCache;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/patient")
@Tag(name="就诊卡管理")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private RedisCache redisCache;

    @PostMapping("add")
    public ResponseResult add(@RequestBody @Validated Patient patient){
        boolean flag = patientService.add(patient);
        if (flag==true){
            return ResponseResult.ok();
        }
        return ResponseResult.fail("添加失败");
    }

    //根据userId查询出所有就诊卡列表
    @GetMapping("findByUserById/{userId}")
    public ResponseResult findUserById(@PathVariable Long userId){
        QueryWrapper<Patient> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        List<Patient> list = patientService.list(wrapper);
        return ResponseResult.ok(list);
    }

    //根据cardNo查询出就诊卡列表
    @GetMapping("findByCardNo/{cardNo}")
    public ResponseResult findUserByCardNo(@PathVariable String cardNo){
        QueryWrapper<Patient> wrapper = new QueryWrapper<>();
        wrapper.eq("card_no",cardNo);
        Patient patient = patientService.getOne(wrapper);
        return ResponseResult.ok(patient);
    }

    //根据就诊卡id查询出就诊人信息
    @GetMapping("findById/{id}")
    public ResponseResult findById(@PathVariable Long id){
        Patient patient = patientService.getById(id);
        return ResponseResult.ok(patient);
    }

    //更新就诊卡信息
    @PostMapping("update")
    public ResponseResult update(@RequestBody @Validated Patient patient){
        patient.setUpdateTime(new Date());
        patientService.updateById(patient);
        return ResponseResult.ok();
    }

    //根据用户id查询第一个就诊人
    @GetMapping("findFirstByUserId/{userId}")
    public ResponseResult findFirstByUserId(@PathVariable Long userId){
        Patient patient = patientService.findFirstByUserId(userId);
        return ResponseResult.ok(patient);
    }

    @GetMapping("findByDocIdAndWorkDate/{docId}")
    public ResponseResult findByDocIdAndWorkDate(@PathVariable Long docId){
        List<Patient> list =  patientService.findByDocIdAndWorkDate(docId);
        return ResponseResult.ok(list);
    }

    //查找待就诊病人队列
    @GetMapping("findFromRedis/{docId}")
    public ResponseResult findFromRedis(@PathVariable Long docId){
        List<Patient> list = patientService.getFromRedis(docId);
        return ResponseResult.ok(list);
    }

    //将患者进行过号处理
    @GetMapping("patientovertime/{orderInfoId}/{docId}")
    public ResponseResult patientovertime(@PathVariable String orderInfoId,@PathVariable Long docId){
        boolean flag = patientService.overtimePatient(orderInfoId,docId);
        return ResponseResult.ok(flag);
    }

    //根据科室编号查询所有过号患者列表
    @GetMapping("findOvertimePatientByDepcode/{depcode}")
    public ResponseResult findOvertimePatientByDepcode(@PathVariable Long depcode){
        //根据今天的日期和科室编号查询过号患者列表
        //构造条件
        QueryWrapper<Schedule> wrapper = new QueryWrapper<>();
        wrapper.eq("depcode",depcode);
        wrapper.eq("work_date",new DateTime(new Date()).toString("yyyy-MM-dd"));
        List<Schedule> list = scheduleService.list(wrapper);

        //存放医生id
        Set<Long> docIdSet = new HashSet<>();
        for (Schedule schedule : list) {
            docIdSet.add(schedule.getDocId());
        }

        List<Patient> patientList = new ArrayList<>();
        //根据医生id查询redis中的过号患者
        for (Long docId : docIdSet) {
            List<Patient> patList =(List<Patient>) redisCache.getCacheObject("doctor:"+docId+":overtime");
            if (StringUtils.isEmpty(patList) &&patList.size()>0){
                patientList.addAll(patList);
            }
        }
        return ResponseResult.ok(patientList);
    }

    //查询过号患者列表
    @GetMapping("findOvertimePatient/{docId}")
    public ResponseResult findOvertimePatient(@PathVariable Long docId){
        List<Patient> list =(List<Patient>) redisCache.getCacheObject("doctor:"+docId+":overtime");

        return ResponseResult.ok(list);
    }

    //对患者进行就诊，在redis中设置就诊病人信息
    @GetMapping("patientVisit/{type}/{orderInfoId}/{docId}")
    public ResponseResult patientVisit(@PathVariable Long type,@PathVariable String orderInfoId,@PathVariable Long docId){
        boolean flag = patientService.visitPatient(type,orderInfoId,docId);
        return ResponseResult.ok(flag);
    }

    //在redis中找出就诊的病人信息
    @GetMapping("findVisitPatient/{docId}")
    public ResponseResult findVisitPatient(@PathVariable Long docId){
        Patient patient = (Patient) redisCache.getCacheObject("doctor:"+docId+":visit");
        return ResponseResult.ok(patient);
    }

    //根据药库id查询出需要取药病人列表
    @GetMapping("findMedicatedPatientList/{userId}")
    public ResponseResult findMedicatedPatientList(@PathVariable Long userId){
        List<Patient> list = patientService.getMedicatedPatientList(userId);
        return ResponseResult.ok(list);
    }

    //根据药库端id获取对应的待取药患者
    @GetMapping("findWaitPatientList/{userId}")
    public ResponseResult findWaitPatientList(@PathVariable Long userId){
        List<Patient> list = patientService.getWaitPatientList(userId);
        return ResponseResult.ok(list);
    }
}
