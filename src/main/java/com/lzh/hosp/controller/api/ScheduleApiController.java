package com.lzh.hosp.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.model.Schedule;
import com.lzh.hosp.service.ScheduleService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/sche")
public class ScheduleApiController {

    @Autowired
    private ScheduleService scheduleService;

    //查询出往后14天的日期
    @GetMapping("/find14Day")
    public ResponseResult find14Day(){
        List<JSONObject> list = new ArrayList<>();
        DateTime dateTime = new DateTime();
        for (int i=0;i<14;i++){
            DateTime date = dateTime.plusDays(i);
            JSONObject json = new JSONObject();
            json.put("key",date.toString("yyyy-MM-dd"));
            json.put("value",date.toString("MM-dd"));
            list.add(json);
        }
        return ResponseResult.ok(list);
    }

    //根据医生id查询出最近的排班
    @GetMapping("/findByDocId/{docId}")
    public ResponseResult findByDocId(@PathVariable Long docId){
        List<Schedule> list = scheduleService.findByDocId(docId);
        return ResponseResult.ok(list);
    }

}
