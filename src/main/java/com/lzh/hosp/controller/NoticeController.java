package com.lzh.hosp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.model.Notice;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.service.NoticeService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/notice")
@Tag(name="公告管理")
public class NoticeController {

    @Autowired
    private NoticeService noticeService;

    //新增公告
    @PostMapping("/add")
    public ResponseResult add(@RequestBody @Validated Notice notice){
        notice.setPublishTime(new Date());
        notice.setUpdateTime(new Date());
        noticeService.save(notice);
        return ResponseResult.ok();
    }

    //根据id查询公告
    @GetMapping("find/{id}")
    public ResponseResult findById(@PathVariable Long id){
        Notice notice = noticeService.getById(id);
        return ResponseResult.ok(notice);
    }

    //条件查询公告
    @GetMapping("find/{page}/{limit}")
    public ResponseResult findNotice(@PathVariable Long page,
                                     @PathVariable Long limit,
                                     String queryInfo){
        IPage<Notice> iPage = noticeService.listNotice(page,limit,queryInfo);
        return ResponseResult.ok(iPage);
    }

    //查询所有公告
    @GetMapping("findAll")
    public ResponseResult findAll(){
        List<Notice> list = noticeService.list();
        return ResponseResult.ok(list);
    }

    //逻辑删除公告
    @DeleteMapping("delete/{id}")
    public ResponseResult deleteById(@PathVariable Long id){
        noticeService.removeById(id);
        return ResponseResult.ok();
    }

    //修改公告
    @PostMapping("update")
    public ResponseResult updateById(@RequestBody Notice notice){
        notice.setUpdateTime(new Date());
        noticeService.updateById(notice);
        return ResponseResult.ok();
    }

}
