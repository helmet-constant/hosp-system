package com.lzh.hosp.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.lzh.hosp.model.Schedule;
import com.lzh.hosp.service.ScheduleService;

import java.util.ArrayList;
import java.util.List;

public class ScheduleListener extends AnalysisEventListener<Schedule> {
    public ScheduleService scheduleService;

    private List<Schedule> list = new ArrayList<>();

    //无参构造
    public ScheduleListener(){}

    //有参构造
    public ScheduleListener(ScheduleService scheduleService){
        this.scheduleService = scheduleService;
    }

    @Override
    public void invoke(Schedule schedule, AnalysisContext analysisContext) {
        list.add(schedule);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        scheduleService.batchAdd(list);
    }
}
