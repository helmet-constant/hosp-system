package com.lzh.hosp.service;

import com.lzh.hosp.model.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 59399
* @description 针对表【dict(组织架构表)】的数据库操作Service
* @createDate 2023-03-15 12:14:44
*/
public interface DictService extends IService<Dict> {

    List<Dict> listChildren(Long id);

    String getDictName(Long value);

    String getParentName(Long value);

    Long getParentValue(Long value);
}
