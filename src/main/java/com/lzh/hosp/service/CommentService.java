package com.lzh.hosp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.model.Comment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author 59399
* @description 针对表【comment】的数据库操作Service
* @createDate 2023-04-14 19:58:22
*/
public interface CommentService extends IService<Comment> {

    boolean saveComment(Comment comment);

    List<Comment> getByDocId(Long docId);

    //好评数量
    Integer getNumberOfPositiveComment(Long docId);

    IPage<Comment> getPageByDocId(Long docId, Long page, Long limit);

    Map<String, Object> getNumberOfCommentAndIntegratedRate(Integer docId);
}
