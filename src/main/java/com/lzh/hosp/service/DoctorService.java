package com.lzh.hosp.service;

import com.lzh.hosp.model.Doctor;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.vo.DoctorQueryVo;

import javax.servlet.http.HttpServletRequest;

/**
* @author 59399
* @description 针对表【doctor】的数据库操作Service
* @createDate 2023-03-16 09:33:52
*/
public interface DoctorService extends IService<Doctor> {

    DoctorQueryVo getDocInfo(Long userId);

    boolean isAuth(HttpServletRequest request);
}
