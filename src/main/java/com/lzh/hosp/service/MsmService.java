package com.lzh.hosp.service;

import java.util.concurrent.ExecutionException;

public interface MsmService {
   boolean send(String phone, String code) throws ExecutionException, InterruptedException;
}
