package com.lzh.hosp.service;

import com.lzh.hosp.model.PageResult;
import com.lzh.hosp.model.dataobject.OperateLogDO;
import com.lzh.hosp.model.dto.OperateLogCreateReqDTO;
import com.lzh.hosp.model.vo.OperateLogExportReqVO;
import com.lzh.hosp.model.vo.OperateLogPageReqVO;

import java.util.List;

/**
 * 操作日志 Service 接口
 *
 * @author 芋道源码
 */
public interface OperateLogService {

    /**
     * 记录操作日志
     *
     * @param createReqDTO 操作日志请求
     */
    void createOperateLog(OperateLogCreateReqDTO createReqDTO);

    /**
     * 获得操作日志分页列表
     *
     * @param reqVO 分页条件
     * @return 操作日志分页列表
     */
    PageResult<OperateLogDO> getOperateLogPage(OperateLogPageReqVO reqVO);

    /**
     * 获得操作日志列表
     *
     * @param reqVO 列表条件
     * @return 日志列表
     */
    List<OperateLogDO> getOperateLogList(OperateLogExportReqVO reqVO);

}
