package com.lzh.hosp.service;

import com.lzh.hosp.model.Patient;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
* @author 59399
* @description 针对表【patient(就诊人表)】的数据库操作Service
* @createDate 2023-03-21 21:23:10
*/
public interface PatientService extends IService<Patient> {

    boolean add(Patient patient);

    Patient findFirstByUserId(Long userId);

    String getPatientName(String cardNo);

    List<Patient> findByDocIdAndWorkDate(Long docId);

    List<Patient> getFromRedis(Long docId);

    boolean overtimePatient(String orderInfoId,Long docId);

    boolean visitPatient(Long type, String orderInfoId,Long docId);

    List<Patient> getMedicatedPatientList(Long userId);

    List<Patient> getWaitPatientList(Long userId);

}
