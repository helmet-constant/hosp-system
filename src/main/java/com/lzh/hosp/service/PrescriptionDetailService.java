package com.lzh.hosp.service;

import com.lzh.hosp.model.PrescriptionDetail;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 59399
* @description 针对表【prescription_detail】的数据库操作Service
* @createDate 2023-04-08 11:02:10
*/
public interface PrescriptionDetailService extends IService<PrescriptionDetail> {

    List<PrescriptionDetail> getByPrescriptionId(String prescriptionId);
}
