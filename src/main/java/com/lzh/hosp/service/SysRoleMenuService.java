package com.lzh.hosp.service;

import com.lzh.hosp.model.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 59399
* @description 针对表【sys_role_menu】的数据库操作Service
* @createDate 2023-05-04 22:18:31
*/
public interface SysRoleMenuService extends IService<SysRoleMenu> {

}
