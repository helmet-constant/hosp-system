package com.lzh.hosp.service;

import com.alibaba.fastjson.JSONObject;
import com.lzh.hosp.model.Doctor;
import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.model.Schedule;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.dto.ScheduleQueryDto;

import java.util.List;

/**
* @author 59399
* @description 针对表【schedule(医生日程安排表)】的数据库操作Service
* @createDate 2023-03-16 20:59:24
*/
public interface ScheduleService extends IService<Schedule> {

    List<Schedule> get(ScheduleQueryDto scheduleQueryDto);

    boolean add(Schedule schedule);

    void batchAdd(List<Schedule> list);

    List<Doctor> findRecently(Long depcode);

    List<Doctor> findByDepcodeAndWorkDate(ScheduleQueryDto scheduleQueryDto);

    List<Schedule> findByDocId(Long docId);

    List<Long> getDocIdByDepCodeAndDate(String date,Long docId);

    List<JSONObject> getRecentDayByDocId(Long docId);

    List<String> get14Day();
}
