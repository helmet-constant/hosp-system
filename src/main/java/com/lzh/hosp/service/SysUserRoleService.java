package com.lzh.hosp.service;

import com.lzh.hosp.model.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 59399
* @description 针对表【sys_user_role】的数据库操作Service
* @createDate 2023-05-04 22:18:46
*/
public interface SysUserRoleService extends IService<SysUserRole> {

}
