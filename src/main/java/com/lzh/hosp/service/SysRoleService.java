package com.lzh.hosp.service;

import com.lzh.hosp.model.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author 59399
* @description 针对表【sys_role】的数据库操作Service
* @createDate 2023-05-04 22:18:20
*/
public interface SysRoleService extends IService<SysRole> {
    List<SysRole> listRolesByUserId(Long userId);
}
