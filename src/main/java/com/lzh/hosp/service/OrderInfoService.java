package com.lzh.hosp.service;

import com.lzh.hosp.model.OrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.vo.OrderInfoQueryVo;

import java.util.List;

/**
* @author 59399
* @description 针对表【order_info(订单表)】的数据库操作Service
* @createDate 2023-03-26 11:15:23
*/
public interface OrderInfoService extends IService<OrderInfo> {

    String add(OrderInfo orderInfo);

    OrderInfo findById(String id);

    List<OrderInfo> getByObj(OrderInfoQueryVo orderInfoQueryVo);

    boolean patientRegistration(String orderInfoId);
}
