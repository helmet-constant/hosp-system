package com.lzh.hosp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.model.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 59399
* @description 针对表【notice】的数据库操作Service
* @createDate 2023-03-14 21:34:11
*/
public interface NoticeService extends IService<Notice> {

    IPage<Notice> listNotice(Long page, Long limit, String title);
}
