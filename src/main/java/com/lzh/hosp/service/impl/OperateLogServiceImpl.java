package com.lzh.hosp.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

import com.lzh.hosp.convert.logger.OperateLogConvert;
import com.lzh.hosp.model.PageResult;
import com.lzh.hosp.model.dataobject.OperateLogDO;
import com.lzh.hosp.model.dto.OperateLogCreateReqDTO;
import com.lzh.hosp.model.vo.OperateLogExportReqVO;
import com.lzh.hosp.model.vo.OperateLogPageReqVO;
import com.lzh.hosp.service.OperateLogService;
import com.lzh.hosp.utils.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.lzh.hosp.model.dataobject.OperateLogDO.JAVA_METHOD_ARGS_MAX_LENGTH;
import static com.lzh.hosp.model.dataobject.OperateLogDO.RESULT_MAX_LENGTH;


@Service
@Validated
@Slf4j
public class OperateLogServiceImpl implements OperateLogService {

//    @Resource
//    private OperateLogMapper operateLogMapper;
//
//    @Resource
//    private AdminUserService userService;

    @Override
    public void createOperateLog(OperateLogCreateReqDTO createReqDTO) {
//        OperateLogDO logDO = OperateLogConvert.INSTANCE.convert(createReqDTO);
//        logDO.setJavaMethodArgs(StrUtils.maxLength(logDO.getJavaMethodArgs(), JAVA_METHOD_ARGS_MAX_LENGTH));
//        logDO.setResultData(StrUtils.maxLength(logDO.getResultData(), RESULT_MAX_LENGTH));
//        operateLogMapper.insert(logDO);
    }

    @Override
    public PageResult<OperateLogDO> getOperateLogPage(OperateLogPageReqVO reqVO) {
//        // 处理基于用户昵称的查询
//        Collection<Long> userIds = null;
//        if (StrUtil.isNotEmpty(reqVO.getUserNickname())) {
//            userIds = convertSet(userService.getUserListByNickname(reqVO.getUserNickname()), AdminUserDO::getId);
//            if (CollUtil.isEmpty(userIds)) {
//                return PageResult.empty();
//            }
//        }
//        // 查询分页
//        return operateLogMapper.selectPage(reqVO, userIds);
        return null;
    }

    @Override
    public List<OperateLogDO> getOperateLogList(OperateLogExportReqVO reqVO) {
//        // 处理基于用户昵称的查询
//        Collection<Long> userIds = null;
//        if (StrUtil.isNotEmpty(reqVO.getUserNickname())) {
//            userIds = convertSet(userService.getUserListByNickname(reqVO.getUserNickname()), AdminUserDO::getId);
//            if (CollUtil.isEmpty(userIds)) {
//                return Collections.emptyList();
//            }
//        }
//        // 查询列表
//        return operateLogMapper.selectList(reqVO, userIds);
        return null;

    }

}