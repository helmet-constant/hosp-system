package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.Medicine;
import com.lzh.hosp.model.PrescriptionDetail;
import com.lzh.hosp.service.MedicineService;
import com.lzh.hosp.service.PrescriptionDetailService;
import com.lzh.hosp.mapper.PrescriptionDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author 59399
* @description 针对表【prescription_detail】的数据库操作Service实现
* @createDate 2023-04-08 11:02:10
*/
@Service
public class PrescriptionDetailServiceImpl extends ServiceImpl<PrescriptionDetailMapper, PrescriptionDetail>
    implements PrescriptionDetailService{

    @Autowired
    private MedicineService medicineService;

    @Override
    public List<PrescriptionDetail> getByPrescriptionId(String prescriptionId) {
        QueryWrapper<PrescriptionDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("prescription_id",prescriptionId);

        List<PrescriptionDetail> prescriptionDetailList = baseMapper.selectList(wrapper);

        //设置其他值
        for (PrescriptionDetail prescriptionDetail : prescriptionDetailList) {
            Medicine medicine = medicineService.getById(prescriptionDetail.getMedId());
            Map<String,Object> params = new HashMap<>();
            params.put("medName",medicine.getMedName());
            params.put("singlePrice",medicine.getPrice());
            params.put("specifications",medicine.getSpecifications());
            prescriptionDetail.setParams(params);
        }
        return prescriptionDetailList;
    }
}




