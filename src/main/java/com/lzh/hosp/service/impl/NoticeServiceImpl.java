package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.Notice;
import com.lzh.hosp.service.NoticeService;
import com.lzh.hosp.mapper.NoticeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
* @author 59399
* @description 针对表【notice】的数据库操作Service实现
* @createDate 2023-03-14 21:34:11
*/
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice>
    implements NoticeService{

    @Override
    public IPage<Notice> listNotice(Long page, Long limit, String title) {
        Page<Notice> noticePage = new Page<>(page,limit);

        //构造条件
        QueryWrapper<Notice> wrapper = new QueryWrapper<>();
        wrapper.like(!StringUtils.isEmpty(title),"title",title);
        Page<Notice> selectPage = baseMapper.selectPage(noticePage, wrapper);

        return selectPage;
    }
}




