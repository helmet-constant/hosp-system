package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.Comment;
import com.lzh.hosp.model.OrderInfo;
import com.lzh.hosp.model.Schedule;
import com.lzh.hosp.service.CommentService;
import com.lzh.hosp.mapper.CommentMapper;
import com.lzh.hosp.service.DoctorService;
import com.lzh.hosp.service.OrderInfoService;
import com.lzh.hosp.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author 59399
* @description 针对表【comment】的数据库操作Service实现
* @createDate 2023-04-14 19:58:22
*/
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment>
    implements CommentService{

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private OrderInfoService orderInfoService;

    @Autowired
    private ScheduleService scheduleService;

    @Override
    public boolean saveComment(Comment comment) {
        //判断这个订单是否已经进行了评论
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("order_info_id",comment.getOrderInfoId());
        Comment com = baseMapper.selectOne(wrapper);
        if (!StringUtils.isEmpty(com)){
            return false;
        }

        //设置其他值
        //设置医生编号
        OrderInfo orderInfo = orderInfoService.getById(comment.getOrderInfoId());
        Schedule schedule = scheduleService.getById(orderInfo.getScheduleId());
        comment.setDocId(schedule.getDocId());
        comment.setCreateTime(new Date());
        comment.setUpdateTime(new Date());
        int insert = baseMapper.insert(comment);
        return insert==1;
    }

    @Override
    public List<Comment> getByDocId(Long docId) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("doc_id",docId);

        List<Comment> list = baseMapper.selectList(wrapper);
        return list;
    }

    @Override
    public Integer getNumberOfPositiveComment(Long docId) {
        Integer numberOfPositiveComment = baseMapper.selectNumberOfPositiveComment(docId);
        if (StringUtils.isEmpty(numberOfPositiveComment)){
            return 0;
        }
        return numberOfPositiveComment;
    }

    @Override
    public IPage<Comment> getPageByDocId(Long docId, Long page, Long limit) {
        IPage<Comment> iPage = new Page<>(page,limit);
        //构造条件
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("doc_id",docId);
        IPage<Comment> commentIPage = baseMapper.selectPage(iPage, wrapper);
        return commentIPage;
    }

    @Override
    public Map<String, Object> getNumberOfCommentAndIntegratedRate(Integer docId) {
        Map<String,Object> params = new HashMap<>();

        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        wrapper.eq("doc_id",docId);
        List<Comment> list = baseMapper.selectList(wrapper);

        //综合评分
        double rate = 0;
        for (Comment comment : list) {
            rate+=comment.getAttitude();
        }
        rate = rate/list.size();

        //评价人数
        Integer numberOfPositiveComment = list.size();

        params.put("numberOfPositiveComment",numberOfPositiveComment);
        params.put("integratedRate",rate);
        return params;
    }
}




