package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.OperateLog;
import com.lzh.hosp.model.SystemOperateLog;
import com.lzh.hosp.service.SystemOperateLogService;
import com.lzh.hosp.mapper.SystemOperateLogMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
* @author 59399
* @description 针对表【system_operate_log(操作日志记录)】的数据库操作Service实现
* @createDate 2023-06-10 18:41:36
*/
@Service
public class SystemOperateLogServiceImpl extends ServiceImpl<SystemOperateLogMapper, SystemOperateLog>
    implements SystemOperateLogService{

    @Override
    public void createOperateLog(OperateLog operateLog) {
        SystemOperateLog systemOperateLog = new SystemOperateLog();
        BeanUtils.copyProperties(operateLog,systemOperateLog);
        systemOperateLog.setCreateTime(new Date());
        systemOperateLog.setUpdateTime(new Date());
        baseMapper.insert(systemOperateLog);
    }
}




