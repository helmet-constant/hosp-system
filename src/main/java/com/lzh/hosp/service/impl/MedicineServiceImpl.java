package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.Dict;
import com.lzh.hosp.model.Medicine;
import com.lzh.hosp.model.dto.MedicineQueryDto;
import com.lzh.hosp.service.DictService;
import com.lzh.hosp.service.MedicineService;
import com.lzh.hosp.mapper.MedicineMapper;
import com.lzh.hosp.service.MedicineWarehouseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
* @author 59399
* @description 针对表【medicine】的数据库操作Service实现
* @createDate 2023-03-15 12:13:46
*/
@Service
public class MedicineServiceImpl extends ServiceImpl<MedicineMapper, Medicine>
    implements MedicineService{

    @Autowired
    private DictService dictService;

    @Autowired
    private MedicineWarehouseService medicineWarehouseService;

    @Override
    public IPage<MedicineQueryDto> list(MedicineQueryDto medicineQueryVo, Long page, Long limit) {
        //药库分类Id
        Long drugWarehouseClassificationId = medicineQueryVo.getDrugWarehouseClassificationId();
        //药品分类Id
        Long classificationCode = medicineQueryVo.getClassificationCode();
        //药品名称
        String medName = medicineQueryVo.getMedName();

        //构造条件
        IPage<Medicine> medicinePage = new Page<>(page,limit);
        QueryWrapper<Medicine> queryWrapper = new QueryWrapper<>();

        //若药品分类Id为空，则根据药库分类查询出所有的药品分类
        if (StringUtils.isEmpty(classificationCode)&&!StringUtils.isEmpty(drugWarehouseClassificationId)){
            List<Dict> dictList = dictService.listChildren(drugWarehouseClassificationId);
            Long[] longs = dictList.stream().map(Dict::getValue).toArray(Long[]::new);
            queryWrapper.in("classification_code",longs);
        }else if (!StringUtils.isEmpty(drugWarehouseClassificationId)){
            queryWrapper.eq(!StringUtils.isEmpty(classificationCode),"classification_code",classificationCode);
        }
        queryWrapper.like(!StringUtils.isEmpty(medName),"med_name",medName);

        IPage<Medicine> medicineIPage = baseMapper.selectPage(medicinePage, queryWrapper);
        List<MedicineQueryDto> medicineQueryVoList = new ArrayList<>();
        for (Medicine medicine : medicineIPage.getRecords()) {
            MedicineQueryDto vo = new MedicineQueryDto();
            //将medicine中的属性赋值给vo
            BeanUtils.copyProperties(medicine,vo);

            //补充vo剩余的属性值
            vo.setClassificationName(dictService.getDictName(vo.getClassificationCode()));
            vo.setDrugWarehouseClassificationName(dictService.getParentName(vo.getClassificationCode()));
            medicineQueryVoList.add(vo);
        }
        IPage<MedicineQueryDto> medicineQueryVoIPage = new Page<>(page,limit);
        medicineQueryVoIPage.setRecords(medicineQueryVoList);
        medicineQueryVoIPage.setTotal(medicineIPage.getTotal());
        return medicineQueryVoIPage;
    }

    @Override
    public List<Medicine> list(MedicineQueryDto medicineQueryVo) {
        //药品分类Id
        Long classificationCode = medicineQueryVo.getClassificationCode();
        //药品名称
        String medName = medicineQueryVo.getMedName();

        //构造条件
        QueryWrapper<Medicine> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(!StringUtils.isEmpty(medName),"med_name",medName);
        queryWrapper.eq(!StringUtils.isEmpty(classificationCode),"classification_code",classificationCode);

        List<Medicine> list = baseMapper.selectList(queryWrapper);
        for (Medicine medicine : list) {
            Integer amount = medicineWarehouseService.getAllAmountByMedId(medicine.getId());
            if (amount<=0){
                list.remove(medicine);
            }
        }
        return list;
    }
}




