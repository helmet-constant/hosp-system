package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.SysUserRole;
import com.lzh.hosp.service.SysUserRoleService;
import com.lzh.hosp.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author 59399
* @description 针对表【sys_user_role】的数据库操作Service实现
* @createDate 2023-05-04 22:18:46
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
    implements SysUserRoleService{

}




