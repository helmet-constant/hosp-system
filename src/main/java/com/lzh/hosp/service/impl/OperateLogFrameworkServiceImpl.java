package com.lzh.hosp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.lzh.hosp.model.OperateLog;
import com.lzh.hosp.model.dto.OperateLogCreateReqDTO;
import com.lzh.hosp.service.OperateLogApi;
import com.lzh.hosp.service.OperateLogFrameworkService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 操作日志 Framework Service 实现类
 *
 * 基于 {@link OperateLogApi} 实现，记录操作日志
 *
 * @author 芋道源码
 */
@RequiredArgsConstructor
@Service
public class OperateLogFrameworkServiceImpl implements OperateLogFrameworkService {

    private final OperateLogApi operateLogApi;

    @Override
    @Async
    public void createOperateLog(OperateLog operateLog) {
        OperateLogCreateReqDTO reqDTO = BeanUtil.copyProperties(operateLog, OperateLogCreateReqDTO.class);
        operateLogApi.createOperateLog(reqDTO);
    }

}