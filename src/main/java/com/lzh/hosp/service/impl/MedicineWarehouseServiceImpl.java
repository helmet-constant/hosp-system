package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.MedicineWarehouse;
import com.lzh.hosp.service.MedicineWarehouseService;
import com.lzh.hosp.mapper.MedicineWarehouseMapper;
import org.springframework.stereotype.Service;

/**
* @author 59399
* @description 针对表【medicine_warehouse】的数据库操作Service实现
* @createDate 2023-03-15 12:16:20
*/
@Service
public class MedicineWarehouseServiceImpl extends ServiceImpl<MedicineWarehouseMapper, MedicineWarehouse>
    implements MedicineWarehouseService{

    @Override
    public Integer getAllAmountByMedId(Long medId) {
        return baseMapper.getAllAmountByMedId(medId);
    }
}




