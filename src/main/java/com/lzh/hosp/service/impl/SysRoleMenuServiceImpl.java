package com.lzh.hosp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzh.hosp.model.SysRoleMenu;
import com.lzh.hosp.service.SysRoleMenuService;
import com.lzh.hosp.mapper.SysRoleMenuMapper;
import org.springframework.stereotype.Service;

/**
* @author 59399
* @description 针对表【sys_role_menu】的数据库操作Service实现
* @createDate 2023-05-04 22:18:31
*/
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
    implements SysRoleMenuService{

}




