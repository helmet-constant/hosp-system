package com.lzh.hosp.service;

import com.lzh.hosp.model.ResponseResult;
import com.lzh.hosp.model.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.dto.LoginDto;

/**
* @author 59399
* @description 针对表【sys_user】的数据库操作Service
* @createDate 2023-05-04 22:17:20
*/
public interface SysUserService extends IService<SysUser> {
    ResponseResult login(LoginDto user);

    SysUser getByUsername(String phone);

    SysUser getByPhone(String phone);

    String getUserAuthorityInfo(Long userId);

    void clearUserAuthorityInfo(Long userId);

    void clearUserAuthorityInfoByRoleId(Long roleId);

    void clearUserAuthorityInfoByMenuId(Long menuId);

    ResponseResult phoneLogin(LoginDto loginDto);
}
