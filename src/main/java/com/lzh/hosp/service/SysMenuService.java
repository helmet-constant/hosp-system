package com.lzh.hosp.service;

import com.lzh.hosp.model.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.dto.SysMenuDto;

import java.util.List;

/**
* @author 59399
* @description 针对表【sys_menu】的数据库操作Service
* @createDate 2023-05-04 22:18:24
*/
public interface SysMenuService extends IService<SysMenu> {
    List<SysMenuDto> getCurrentUserNav();

    List<SysMenu> tree();
}
