package com.lzh.hosp.service;

import com.lzh.hosp.model.Prescription;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.PrescriptionDetail;
import com.lzh.hosp.model.dto.PrescriptionQueryDto;

import java.util.List;

/**
* @author 59399
* @description 针对表【prescription】的数据库操作Service
* @createDate 2023-04-08 11:02:01
*/
public interface PrescriptionService extends IService<Prescription> {

    boolean savePrescription(Prescription prescription, List<PrescriptionDetail> prescriptionDetailList);

    List<Prescription> getPrescription(PrescriptionQueryDto prescriptionQueryDto);

    boolean pay(String prescriptionId);

    boolean finishMedicationCollection(String prescriptionId,Long userId);

    boolean finishTakeMedicine(String prescriptionId, Long userId);

    Integer getNumberOfVisitsReceived(Long docId);

}
