package com.lzh.hosp.service;

import com.lzh.hosp.model.OperateLog;
import com.lzh.hosp.model.SystemOperateLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 59399
* @description 针对表【system_operate_log(操作日志记录)】的数据库操作Service
* @createDate 2023-06-10 18:41:36
*/
public interface SystemOperateLogService extends IService<SystemOperateLog> {
    /**
     * 记录操作日志
     *
     * @param operateLog 操作日志请求
     */
    void createOperateLog(OperateLog operateLog);
}
