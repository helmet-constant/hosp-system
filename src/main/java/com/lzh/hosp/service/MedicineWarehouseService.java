package com.lzh.hosp.service;

import com.lzh.hosp.model.MedicineWarehouse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 59399
* @description 针对表【medicine_warehouse】的数据库操作Service
* @createDate 2023-03-15 12:16:20
*/
public interface MedicineWarehouseService extends IService<MedicineWarehouse> {
    //根据药品id查询出所有的库存
    Integer getAllAmountByMedId(Long medId);
}
