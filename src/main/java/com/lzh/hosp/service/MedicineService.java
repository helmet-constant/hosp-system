package com.lzh.hosp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.model.Medicine;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lzh.hosp.model.dto.MedicineQueryDto;

import java.util.List;

/**
* @author 59399
* @description 针对表【medicine】的数据库操作Service
* @createDate 2023-03-15 12:13:46
*/
public interface MedicineService extends IService<Medicine> {

    //带分页条件查询药品
    IPage<MedicineQueryDto> list(MedicineQueryDto medicineQueryVo, Long page, Long limit);

    //不带分页条件查询药品
    List<Medicine> list(MedicineQueryDto medicineQueryVo);
}
