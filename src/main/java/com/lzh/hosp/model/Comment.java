package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 
 * @TableName comment
 */
@Schema
@TableName(value ="comment")
@Data
public class Comment implements Serializable {
    /**
     * 
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 就诊卡号
     */
    @TableField(value = "card_no")
    private String cardNo;

    /**
     * 医生编号
     */
    @TableField(value = "doc_id")
    private Long docId;

    /**
     * 订单编号
     */
    @TableField(value = "order_info_id")
    private String orderInfoId;

    /**
     * 内容
     */
    @TableField(value = "content")
    private String content;

    /**
     * 服务态度
     */
    @TableField(value = "attitude")
    private Integer attitude;

    /**
     * 服务质量
     */
    @TableField(value = "quanlity")
    private Integer quanlity;

    /**
     * 服务效果
     */
    @TableField(value = "effect")
    private Integer effect;

    /**
     * 满意程度
     */
    @TableField(value = "degree")
    private String degree;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 逻辑删除(1:已删除，0:未删除)
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}