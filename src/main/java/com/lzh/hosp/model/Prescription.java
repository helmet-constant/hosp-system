package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 
 * @TableName prescription
 */
@Schema
@TableName(value ="prescription")
@Data
public class Prescription implements Serializable {
    /**
     * 处方编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 订单编号
     */
    @TableField(value = "order_info_id")
    @NotBlank(message = "订单编号不能为空")
    private String orderInfoId;

    /**
     * 就诊卡号
     */
    @NotBlank(message = "就诊卡号不能为空")
    @TableField(value = "card_no")
    private String cardNo;

    /**
     * 就诊医生编号
     */
    @NotNull(message = "医生编号不能为空")
    @TableField(value = "doc_id")
    private Integer docId;

    /**
     * 体温
     */
    @TableField(value = "temperature")
    private String temperature;

    /**
     * 脉搏
     */
    @TableField(value = "pluse")
    private String pluse;

    /**
     * 心率
     */
    @TableField(value = "heart_rate")
    private String heartRate;

    /**
     * 血压
     */
    @TableField(value = "blood_pressure")
    private String bloodPressure;

    /**
     * 主诉
     */
    @TableField(value = "chief_complaint")
    private String chiefComplaint;

    /**
     * 个人史
     */
    @TableField(value = "personal_history")
    private String personalHistory;

    /**
     * 既往史
     */
    @TableField(value = "past_history")
    private String pastHistory;

    /**
     * 体查
     */
    @TableField(value = "body_check")
    private String bodyCheck;

    /**
     * 初步诊断
     */
    @TableField(value = "initial_diagnosis")
    private String initialDiagnosis;

    /**
     * 意见或建议
     */
    @TableField(value = "advice")
    private String advice;

    /**
     * 诊断费用
     */
    @TableField(value = "diagnostic_cost")
    private Double diagnosticCost;

    /**
     * 总费用
     */
    @TableField(value = "total_price")
    private Double totalPrice;

    /**
     * 处方状态
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 药房编号
     */
    @TableField(value = "drugstore_id")
    private Long drugstoreId;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private Map<String,Object> params;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}