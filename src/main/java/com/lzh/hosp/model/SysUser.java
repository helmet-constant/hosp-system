package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 
 * @TableName sys_user
 */
@TableName(value ="sys_user")
@Data
@Schema
public class SysUser implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    @TableField(value = "phone")
    @Pattern(regexp = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$",message = "手机格式错误")
    @NotBlank(message = "手机号不能为空")
    private String phone;

    /**
     * 
     */
    @TableField(value = "username")
    private String username;

    /**
     * 
     */
    @TableField(value = "password")
    @Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$",message = "密码为6-20位的英文数字组合")
    private String password;

    /**
     * 
     */
    @TableField(value = "avatar")
    private String avatar;

    /**
     * 
     */
    @TableField(value = "email")
    private String email;

    /**
     * 
     */
    @TableField(value = "city")
    private String city;

    /**
     * 
     */
    @TableField(value = "created")
    private LocalDateTime created;

    /**
     * 
     */
    @TableField(value = "updated")
    private LocalDateTime updated;

    /**
     * 
     */
    @TableField(value = "last_login")
    private LocalDateTime lastLogin;

    /**
     * 
     */
    @TableField(value = "statu")
    private Integer statu;

    @TableField(exist = false)
    private List<SysRole> sysRoles = new ArrayList<>();

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}