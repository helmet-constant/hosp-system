package com.lzh.hosp.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema
public class Message {
    String action;

    Long receiverCode;

    Long receiverDepcode;

    Map<String,Object> params;
}
