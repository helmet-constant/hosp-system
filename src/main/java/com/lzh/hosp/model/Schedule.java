package com.lzh.hosp.model;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 医生日程安排表
 * @TableName schedule
 */
@TableName(value ="schedule")
@Schema
@Data
public class Schedule implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id",type = IdType.AUTO)
    @ExcelIgnore
    private Long id;

    /**
     * 科室编号
     */
    @TableField(value = "depcode")
    @ExcelProperty(value = "depcode")
    @NotNull(message = "科室编号不能为空")
    private Long depcode;

    /**
     * 医生编号
     */
    @TableField(value = "doc_id")
    @ExcelProperty(value = "docId")
    @NotNull(message = "医生编号不能为空")
    private Long docId;

    /**
     * 安排日期
     */
    @TableField(value = "work_date")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ExcelProperty(value = "workDate")
    @NotNull(message = "安排日期不能为空")
    private Date workDate;

    /**
     * 安排时间（0：上午 1：下午）
     */
    @TableField(value = "work_time")
    @ExcelProperty(value = "workTime")
    @NotNull(message = "安排时间不能为空")
    private Integer workTime;

    /**
     * 可预约数
     */
    @TableField(value = "reserved_number")
    @ExcelProperty(value = "reservedNumber")
    @NotNull(message = "可预约数不能为空")
    private Integer reservedNumber;

    /**
     * 剩余预约数
     */
    @TableField(value = "available_number")
    @ExcelIgnore
    private Integer availableNumber;

    /**
     * 挂号费
     */
    @TableField(value = "amount")
    @ExcelProperty(value = "amount")
    @NotNull(message = "挂号费不能为空")
    private Double amount;

    /**
     * 排班状态（-1：停诊 0：停约 1：可约）
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ExcelIgnore
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ExcelIgnore
    private Date updateTime;

    /**
     * 逻辑删除(1:已删除，0:未删除)
     */
    @TableField(value = "is_deleted")
    @ExcelIgnore
    private Integer isDeleted;

    @Version
    @TableField(fill = FieldFill.INSERT)
    private Integer version;

    @TableField(exist = false)
    @ExcelIgnore
    Map<String,Object> params;

    @TableField(exist = false)
    @ExcelIgnore
    private static final long serialVersionUID = 1L;
}