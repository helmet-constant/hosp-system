package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 
 * @TableName prescription_detail
 */
@Schema
@TableName(value ="prescription_detail")
@Data
public class PrescriptionDetail implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 处方编号
     */
    @TableField(value = "prescription_id")
    private String prescriptionId;

    /**
     * 药品编号
     */
    @TableField(value = "med_id")
    private Long medId;

    /**
     * 数量
     */
    @TableField(value = "amount")
    private Integer amount;

    /**
     * 总价
     */
    @TableField(value = "total_price")
    private Double totalPrice;

    /**
     * 用量
     */
    @TableField(value = "consumption")
    private String consumption;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    @TableField(exist = false)
    private Map<String,Object> params;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}