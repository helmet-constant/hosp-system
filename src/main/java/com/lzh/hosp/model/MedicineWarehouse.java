package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 
 * @TableName medicine_warehouse
 */
@Schema
@TableName(value ="medicine_warehouse")
@Data
public class MedicineWarehouse implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 药品id
     */
    @TableField(value = "med_id")
    @NotNull(message = "药品编号不能为空")
    private Long medId;

    /**
     * 创建时间
     */
    @TableField(value = "created_time")
    private Date createdTime;

    /**
     * 数量
     */
    @TableField(value = "amount")
    @NotNull(message = "数量不能为空")
    private Integer amount;

    /**
     * 过期时间
     */
    @TableField(value = "term_of_validity")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @NotNull(message = "过期时间不能为空")
    private Date termOfValidity;

    @TableField(exist = false)
    private Map<String,Object> params;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}