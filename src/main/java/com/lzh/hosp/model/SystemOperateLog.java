package com.lzh.hosp.model;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

/**
 * 操作日志记录
 * @TableName system_operate_log
 */
@TableName(value ="system_operate_log")
@ToString
@Schema
@Data
public class SystemOperateLog implements Serializable {
    /**
     * 日志主键
     */
    @ExcelProperty("编号")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 链路追踪编号
     */
    @ExcelIgnore
    @TableField(value = "trace_id")
    private String traceId;

    /**
     * 用户编号
     */
    @ExcelProperty("用户编号")
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 用户类型
     */
    @ExcelProperty("用户类型")
    @TableField(value = "user_type")
    private Integer userType;

    /**
     * 模块标题
     */
    @ExcelProperty("模块标题")
    @TableField(value = "module")
    private String module;

    /**
     * 操作名
     */
    @ExcelProperty("操作名")
    @TableField(value = "name")
    private String name;

    /**
     * 操作分类
     */
    @ExcelProperty("操作分类")
    @TableField(value = "type")
    private Integer type;

    /**
     * 操作内容
     */
    @ExcelProperty("操作内容")
    @TableField(value = "content")
    private String content;

    /**
     * 拓展字段
     */
    @ExcelProperty("拓展字段")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map<String, Object> exts;

    /**
     * 请求方法名
     */
    @ExcelProperty("请求方法名")
    @TableField(value = "request_method")
    private String requestMethod;

    /**
     * 请求地址
     */
    @ExcelProperty("请求地址")
    @TableField(value = "request_url")
    private String requestUrl;

    /**
     * 用户 IP
     */
    @ExcelProperty("用户 IP")
    @TableField(value = "user_ip")
    private String userIp;

    /**
     * 浏览器 UA
     */
    @ExcelProperty("浏览器 UA")
    @TableField(value = "user_agent")
    private String userAgent;

    /**
     * Java 方法名
     */
    @ExcelProperty("Java 方法名")
    @TableField(value = "java_method")
    private String javaMethod;

    /**
     * Java 方法的参数
     */
    @ExcelProperty("Java 方法的参数")
    @TableField(value = "java_method_args")
    private String javaMethodArgs;

    /**
     * 操作时间
     */
    @ExcelProperty("操作时间")
    @TableField(value = "start_time")
    private LocalDateTime startTime;

    /**
     * 执行时长
     */
    @ExcelProperty("执行时长")
    @TableField(value = "duration")
    private Integer duration;

    /**
     * 结果码
     */
    @ExcelProperty("结果码")
    @TableField(value = "result_code")
    private Integer resultCode;

    /**
     * 结果提示
     */
    @ExcelProperty("结果提示")
    @TableField(value = "result_msg")
    private String resultMsg;

    /**
     * 结果数据
     */
    @ExcelProperty("结果数据")
    @TableField(value = "result_data")
    private String resultData;

    /**
     * 创建者
     */
    @ExcelProperty("创建者")
    @TableField(value = "creator")
    private String creator;

    /**
     * 创建时间
     */
    @ExcelProperty("创建时间")
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新者
     */
    @ExcelProperty("更新者")
    @TableField(value = "updater")
    private String updater;

    /**
     * 更新时间
     */
    @ExcelProperty("更新时间")
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 是否删除
     */
    @ExcelIgnore
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}