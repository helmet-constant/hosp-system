package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Map;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 
 * @TableName doctor
 */
@TableName(value ="doctor")
@Data
@Schema
public class Doctor implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账户id（用于登录）
     */
    @TableField(value = "account_id")
    private Long accountId;

    /**
     * 职称
     */
    @TableField(value = "professional_title")
    @NotBlank(message = "职称不能为空")
    private String professionalTitle;

    /**
     * 医师姓名
     */
    @TableField(value = "username")
    @NotBlank(message = "姓名不能为空")
    private String username;

    /**
     * 性别
     */
    @TableField(value = "gender")
    @NotNull(message = "性别不能为空")
    private Integer gender;

    /**
     * 姓名
     */
    @TableField(value = "age")
    @NotNull(message = "年龄不能为空")
    private Integer age;

    /**
     * 擅长领域
     */
    @TableField(value = "expertise")
    @NotBlank(message = "擅长领域不能为空")
    private String expertise;

    /**
     * 科室编号
     */
    @TableField(value = "depcode")
    @NotNull(message = "科室编号不能为空")
    private Long depcode;

    /**
     * 头像路径
     */
    @TableField(value = "image_url")
    @NotBlank(message = "头像不能为空")
    private String imageUrl;

    /**
     * 认证状态
     */
    @TableField(value = "auth_status")
    private Integer authStatus;
    /**
     *是否删除
     */
    @TableLogic
    @TableField("is_deleted")
    private String isDeleted;

    @TableField(exist = false)
    Map<String,Object> params;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}