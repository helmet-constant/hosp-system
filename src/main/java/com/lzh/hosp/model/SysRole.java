package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 
 * @TableName sys_role
 */
@TableName(value ="sys_role")
@Data
@Schema
public class SysRole implements Serializable {
    /**
     * 
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    @TableField(value = "name")
    private String name;

    /**
     * 
     */
    @TableField(value = "code")
    private String code;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 
     */
    @TableField(value = "created")
    private LocalDateTime created;

    /**
     * 
     */
    @TableField(value = "updated")
    private LocalDateTime updated;

    /**
     * 
     */
    @TableField(value = "statu")
    private Integer statu;


    @TableField(exist = false)
    private List<Long> menuIds = new ArrayList<>();

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}