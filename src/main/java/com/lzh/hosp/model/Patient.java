package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 就诊人表
 * @TableName patient
 */
@TableName(value ="patient")
@Schema
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Patient implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @TableField(value = "name")
    private String name;

    /**
     * 就诊卡号
     */
    @TableField(value = "card_no")
    private String cardNo;

    /**
     * 证件类型
     */
    @TableField(value = "certificates_type")
    private Integer certificatesType;

    /**
     * 证件编号
     */
    @TableField(value = "certificates_no")
    private String certificatesNo;

    /**
     * 性别
     */
    @TableField(value = "sex")
    private Integer sex;

    /**
     * 年龄
     */
    @TableField(value = "age")
    private Integer age;

    /**
     * 出生年月
     */
    @TableField(value = "birthdate")
    private Date birthdate;

    /**
     * 手机
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 省code
     */
    @TableField(value = "province_code")
    private String provinceCode;

    /**
     * 市code
     */
    @TableField(value = "city_code")
    private String cityCode;

    /**
     * 区code
     */
    @TableField(value = "district_code")
    private String districtCode;

    /**
     * 详情地址
     */
    @TableField(value = "address")
    private String address;

    /**
     * 是否有医保
     */
    @TableField(value = "is_insure")
    private Integer isInsure;

    /**
     * 状态（0：默认 1：已认证）
     */
    @TableField(value = "status")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 逻辑删除(1:已删除，0:未删除)
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private Map<String,Object> params;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}