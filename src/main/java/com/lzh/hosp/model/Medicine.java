package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 
 * @TableName medicine
 */
@TableName(value ="medicine")
@Data
@AllArgsConstructor
@Schema
@NoArgsConstructor
public class Medicine implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 药品名称
     */
    @TableField(value = "med_name")
    @NotBlank(message = "药品名称不能为空")
    private String medName;

    /**
     * 药品名称
     */
    @TableField(value = "price")
    @NotNull(message = "价格不能为空")
    private Double price;

    /**
     * 规格
     */
    @TableField(value = "specifications")
    @NotBlank(message = "规格不能为空")
    private String specifications;

    /**
     * 药品分类id
     */
    @TableField(value = "classification_code")
    @NotNull(message = "药品分类不能为空")
    private Long classificationCode;

    /**
     * 厂家名称
     */
    @TableField(value = "factory")
    @NotBlank(message = "厂商不能为空")
    private String factory;

    /**
     * 创建时间
     */
    @TableField(value = "created_time")
    private Date createdTime;

    /**
     * 发布时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 是否删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}