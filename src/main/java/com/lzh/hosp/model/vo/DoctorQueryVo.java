package com.lzh.hosp.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoctorQueryVo {
    /**
     *
     */
    private Long id;

    /**
     *
     */
    private Long accountId;

    /**
     *
     */
    private String username;

    private String professionalTitle;

    /**
     *
     */
    private String gender;

    /**
     *
     */
    private Integer age;

    /**
     *
     */
    private String expertise;

    private Integer authStatus;

    /**
     *
     */
    private Long depcode;
    //科室名称
    private String depName;
    private String imageUrl;
}
