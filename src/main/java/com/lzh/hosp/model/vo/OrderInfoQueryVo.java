package com.lzh.hosp.model.vo;

import lombok.Data;

import java.util.Date;

@Data
public class OrderInfoQueryVo {
    //就诊卡号
    String cardNo;

    //预约时间
    Date dateRange[];
}
