package com.lzh.hosp.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 订单表
 * @TableName order_info
 */
@TableName(value ="order_info")
@Data
@Schema
public class OrderInfo implements Serializable {
    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 排班id
     */
    @NotNull(message = "排班编号不能为空")
    @TableField(value = "schedule_id")
    private Long scheduleId;

    /**
     * 就诊人id
     */
    @NotBlank(message = "就诊卡号不能为空")
    @TableField(value = "card_no")
    private String cardNo;

    /**
     * 取号地点
     */
    @TableField(value = "fetch_address")
    private String fetchAddress;

    /**
     * 挂号费
     */
    @TableField(value = "amount")
    private Double amount;

    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    private Date payTime;

    /**
     * 退号时间
     */
    @TableField(value = "quit_time")
    private Date quitTime;

    /**
     * 退号原因
     */
    @TableField(value = "cancel_reason")
    private String cancelReason;

    /**
     * 订单状态
     */
    @TableField(value = "order_status")
    private Integer orderStatus;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm",timezone = "GMT+8")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 逻辑删除(1:已删除，0:未删除)
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private Map<String,Object> params;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}