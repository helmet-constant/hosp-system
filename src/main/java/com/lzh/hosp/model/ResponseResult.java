package com.lzh.hosp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.NoArgsConstructor;


@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Schema
public class ResponseResult<T> {
     private Integer code;
     private String msg;
     private T data;

    public ResponseResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
   public ResponseResult(Integer code, T data) {
        this.code = code;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ResponseResult ok(Object data) {
        ResponseResult responseResult = new ResponseResult<>(200,"请求成功",data);
        return responseResult;
    }
    public static ResponseResult ok() {
        ResponseResult responseResult = new ResponseResult<>(200,"请求成功",null);
        return responseResult;
    }
    public static ResponseResult fail(String message) {
        ResponseResult responseResult = new ResponseResult<>(400,message,null);
        return responseResult;
    }
}
