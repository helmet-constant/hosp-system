package com.lzh.hosp.model.dto;

import lombok.Data;

@Data
public class DoctorUpdateAuthDto {
    private Long id;
    private Integer authStatus;
    private String advice;
}
