package com.lzh.hosp.model.dto;

import lombok.Data;

@Data
public class ScheduleQueryDto {
    //科室id
    private Long depcode;

    //医生id
    private Long docId;

    //医生名字
    private String docname;

    //排班日期
    private String workDate;

    //排版时间段
    private Integer[] workTime;
}
