package com.lzh.hosp.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
public class PassDto implements Serializable {

	@NotBlank(message = "新密码不能为空")
	@Pattern(regexp = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$",message = "密码为6-20位的英文数字组合")
	private String password;

	@NotBlank(message = "旧密码不能为空")
	private String currentPass;
}
