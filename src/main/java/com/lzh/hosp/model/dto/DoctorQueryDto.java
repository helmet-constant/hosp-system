package com.lzh.hosp.model.dto;

import lombok.Data;

@Data
public class DoctorQueryDto {
    private String username;
    private Integer authStatus;
    private Long depcode;
}
