package com.lzh.hosp.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName medicine
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicineQueryDto implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private String medName;

    /**
     * 规格
     */
    private String specifications;

    /**
     * 
     */
    private Long classificationCode;

    //药品分类名称
    private String classificationName;

    //药库分类名称
    private String drugWarehouseClassificationName;

    //药库分类id
    private Long drugWarehouseClassificationId;

    //厂商
    private String factory;

    //价格
    private double price;



    /**
     * 
     */
    private Date createdTime;

    /**
     * 
     */
    private Date updateTime;

    private static final long serialVersionUID = 1L;
}