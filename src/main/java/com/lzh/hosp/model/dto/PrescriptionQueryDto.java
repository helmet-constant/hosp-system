package com.lzh.hosp.model.dto;

import lombok.Data;

@Data
public class PrescriptionQueryDto {

    private String id;
    private Long userId;
    private String cardNo;
    private String createTime;
    private Integer status;

}
