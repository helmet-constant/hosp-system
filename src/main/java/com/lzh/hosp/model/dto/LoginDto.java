package com.lzh.hosp.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginDto {
    String phone;
    String password;
    String code;
}
