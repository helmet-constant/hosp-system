package com.lzh.hosp.model;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 错误码对象
 *
 *
 * TODO 错误码设计成对象的原因，为未来的 i18 国际化做准备
 */
@Data
@Schema
public class ErrorCode {

    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误提示
     */
    private final String msg;

    public ErrorCode(Integer code, String message) {
        this.code = code;
        this.msg = message;
    }

}
