package com.lzh.hosp.filter;

import com.lzh.hosp.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Component
public class DoctorAuthFilter extends HttpFilter {
    @Autowired
    private DoctorService doctorService;

    public static String URL_LIST[]={"/prescription/generate","/sys/role/list"};


    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        boolean contains = Arrays.asList(URL_LIST).contains(request.getRequestURI());
        if (contains){
            boolean auth = doctorService.isAuth(request);
            if (auth){
                chain.doFilter(request, response);
                return;
            }else{
                throw new RuntimeException("尚未通过认证，请联系管理员");
            }
        }else {
            chain.doFilter(request, response);
            return;
        }
    }
}
