package com.lzh.hosp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement//开启事务回滚
public class HospSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospSystemApplication.class, args);
    }

}
