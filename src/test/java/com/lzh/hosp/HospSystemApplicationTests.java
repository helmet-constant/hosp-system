package com.lzh.hosp;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lzh.hosp.listener.ScheduleListener;
import com.lzh.hosp.mapper.ScheduleMapper;
import com.lzh.hosp.model.*;
import com.lzh.hosp.service.*;
import com.lzh.hosp.utils.CommonUtils;
import com.lzh.hosp.utils.DateUtils;
import com.lzh.hosp.utils.RedisCache;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.ParseException;
import java.util.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HospSystemApplicationTests {

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private DictService dictService;

    @Autowired
    private MedicineService medicineService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ScheduleMapper scheduleMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private MedicineWarehouseService medicineWarehouseService;

    @Test
    void contextLoads() {
    }

    @Test
    void t1(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String pwd = bCryptPasswordEncoder.encode("admin");
        System.out.println(pwd);
    }

    @Test
    void t6(){
        List<DateTime> list = new ArrayList<>();
        for(int i=1;i<=6;i++){
            DateTime dateTime = new DateTime(new Date());
            DateTime date = dateTime.plusDays(i);
            list.add(date);
        }
        System.out.println(list);
    }

    @Test
    void t7(){
        boolean effectiveDay = DateUtils.isEffectiveDay(new DateTime("2023-4-1").toDate());
        System.out.println(effectiveDay);
    }

    @Test
    void t8(){
        scheduleService.findRecently(126L);
    }

    @Test
    void t9(){
        try {
            String card = "44190020010711434";
            int sexFromIdCard = CommonUtils.getSexFromIdCard(card);
            Date birthDayFromIdCard = CommonUtils.getBirthDayFromIdCard(card);
            int ageForIdcard = CommonUtils.getAgeForIdcard(card);
            System.out.println(sexFromIdCard);
            System.out.println(birthDayFromIdCard);
            System.out.println(ageForIdcard);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void t10(){
        Object cacheObject = redisCache.getCacheObject("5f68d20bde2189faf8a9e75af9389320" + "orderInfo");
        System.out.println(cacheObject);
//        redisCache.deleteObject("5f68d20bde2189faf8a9e75af9389320" + "orderInfo");
        Long a947a123c0a3c29d13759322a84e9d83 = redisCache.getTTL("a947a123c0a3c29d13759322a84e9d83");
        System.out.println(a947a123c0a3c29d13759322a84e9d83);
    }

    @Test
    void t11(){
        Deque<String> deque = new LinkedList<>();
        deque.add("as");
        redisCache.setCacheObject("wait:"+1,deque);
    }

    @Test
    void t12(){
        Set<Patient> set = new HashSet<>();
        for (int i =0;i<2;i++){
            Patient patient = new Patient();
            patient.setId(12L);
            set.add(patient);
        }
        System.out.println(set);
    }

    @Test
    void t14(){
        QueryWrapper<MedicineWarehouse> wareHouseWrapper = new QueryWrapper<>();
        wareHouseWrapper.eq("med_id",1);
        wareHouseWrapper.orderByAsc("term_of_validity");
        List<MedicineWarehouse> medicineWarehouseList = medicineWarehouseService.list(wareHouseWrapper);
        System.out.println(medicineWarehouseList);
    }

    @Test
    void t15(){
        //设置读取文件夹的位置
        String filepath = "D:\\desktop\\test.xlsx";
        //调用EasyExcel里面的方法实现读操作
        EasyExcel.read(filepath,Schedule.class, new ScheduleListener()).sheet().doRead();
    }

    @Test
    void t16(){
        scheduleService.getRecentDayByDocId(2L);
    }

    @Autowired
    private CommentService commentService;

    @Test
    void t17(){
        IPage<Comment> pageByDocId = commentService.getPageByDocId(2L, 3L, 1L);
        System.out.println(pageByDocId.getRecords());
    }

    @Test
    void t18(){
        for (int i=0;i<10;i++){
            String substring = UUID.randomUUID().toString().replace("-", "").substring(0, 8);
            System.out.println(substring);
        }
    }

    @Test
    void t19(){
        List<Integer> workTimeList = new ArrayList<>();
        workTimeList.add(1);
        workTimeList.add(2);
        workTimeList.add(3);
        List<Long> list = scheduleMapper.selectIllegal("2023-5-31", workTimeList);
        System.out.println(list);
    }

    @Test
    void t20(){

    }

}
