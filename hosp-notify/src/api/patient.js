import request from '@/utils/request'

export default {
    findPatientByDocId(docId){
        return request({
            url:`patient/findFromRedis/${docId}`,
            method:'get'
        })
    },
    findVisitByDocId(docId) {
        return request({
            url:`patient/findVisitPatient/${docId}`,
            method:'get'
        })
    },
    findMedicatedPatientList(drugstoreId) {
        return request({
            url:`patient/findMedicatedPatientList/${drugstoreId}`,
            method:'get'
        })
    },
    findWaitPatientList(drugstoreId) {
        return request({
            url:`patient/findWaitPatientList/${drugstoreId}`,
            method:'get'
        })
    },
    findOvertimePatientByDepcode(depcode){
        return request({
            url:`patient/findOvertimePatientByDepcode/${depcode}`,
            method:'get'
        })    }
}
