import request from '@/utils/request'

export default {
    findDocList(searchObj){
        return request({
            url:`sche/findByDepcodeAndWorkDate`,
            method:'get',
            params:searchObj
        })
    }
}
