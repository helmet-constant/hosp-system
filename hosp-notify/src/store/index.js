import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    //数据，相当于data
    state: {
        depcode:''
    },
    getters: {

    },
    //里面定义方法，操作state方发
    mutations: {
        updateDepcode(state,depcode){
            state.depcode = depcode
        }
    },
    // 操作异步操作mutation
    actions: {

    },
    modules: {

    },
})