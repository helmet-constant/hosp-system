import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const routes = [
    {
        path:'/login',
        name:'login',
        component: () => import('@/views/login/')
    },
    {
        path:'/home',
        name:'home',
        component:()=>import('@/views/home/')
    },
    {
        path:'/drugstore',
        name: 'drugstoreDesktop',
        component:()=>import('@/views/drugstore')
    }

]

const router = new VueRouter({
    mode: 'history',
    base: "/",
    routes
})
//
// router.beforeEach((to, from, next) => {
//     if (to.name === 'login' || !to.meta.requiresAuth) {
//         return next()
//     }
//
//     if (store.state.user) {
//         return next()
//     }
//
//     Dialog.confirm({
//         title: '该功能需要登录，确认登录吗？'
//     }).then(() => {
//         next({
//             name: 'login',
//             query: {
//                 redirect: from.fullPath
//             }
//         })
//     }).catch(() => {
//         // on cancel
//     })
// })

export default router
