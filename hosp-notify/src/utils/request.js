import axios from "axios"
const request = axios.create({
  baseURL:'http://localhost:8080/'
})

//请求拦截器
// 请求拦截器
// Add a request interceptor
request.interceptors.request.use(function (config) {
  // Do something before request is sent
  // const { user } = store.state

  // 如果用户已登录，统一给接口设置 token 信息
  if (config.url=='user/login'){
    return config
  }

  // if (user) {
  //   config.headers.token = `${user.token}`
  // }

  // 处理完之后一定要把 config 返回，否则请求就会停在这里
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})
//响应拦截器
request.interceptors.response.use(function (config){

  return config;
},function (error) {
  // Do something with request error
  return Promise.reject(error)
})

export default request
