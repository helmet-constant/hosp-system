import moment from "moment"

function dateFormat(data) {
    return moment(new Date(data).getTime()).format('YYYY-MM-DD')
}

export {

    dateFormat

}
