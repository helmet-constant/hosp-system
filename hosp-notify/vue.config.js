const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  devServer: {
    port:8081,
    // proxy:{
    //   '/aliyun':{
    //     target:'https://nls-gateway-cn-shanghai.aliyuncs.com', //后台接口地址
    //     ws:false, //如果要代理 websockets，配置这个参数
    //     secure:true,     //如果是https接口，需要配置这个参数
    //     changeOrigin:true, //是否跨域
    //     pathRewrite:{
    //       '^/aliyun':''
    //     }
    //   }
    // }
  }
})
